# phobos testing

**recommendation**

- [build testing](#build-testing)
- [unit testing](#unit-testing)
- Create a [FreeRTOS POSIX port](#freertos-posix-port) of phobos-hdp, and run [FreeRTOS WIFI Library tests](#freertos-wifi-tests)
- [hardware testbed](#hardware-testbed-system-testing) running `aip_wtf` and [FreeRTOS WIFI Library tests](#freertos-wifi-tests)
- build, unit, and emulated FreeRTOS should be integrated into CI

**Things we would like to do given more resources:**

- Create a [FreeRTOS qemu port](#freertos-qemu-port) and run [FreeRTOS WIFI Library tests](#freertos-wifi-tests)
    - correct target architecture
    - exercise `wpa_supplicant` `aip` driver
- hack up a thinmac driver for the hostap [hwsim tests](#hwsim-tests)

## build testing

- CI should run on every PR, at minimum ensure:
    - hostap
        - buildroot and phobos-hdp builds
        - x86_64 builds
    - phobos-hdp
        - builds
    - build passes without warnings
    - lint checker passes
 

## unit testing

New modules in hostap should be unit testable.

**module inventory**:

- os_notify
- iot_wifi
- wpa_freertos
- buffer management
- wpa_tx
    - encapsulation
- wpa_rx
    - decapsulation

## hwsim tests

We may leverage hwsim tests with a new driver simulating a thinmac hardware,
but implemented using hwsim monitor interface injection. Unclear if this is
feasible, but would be valuable.

| pros                                     | cons                                      |
|------------------------------------------|-------------------------------------------|
| can leverage existing (S1G!) hwsim tests | not sure if the data path is workable     |
| packet captures                          | would require changes in linux            |
|                                          | test driver would not exercise iot_wifi.h |
|                                          | iw, debugfs, etc. won't work at all       |

## FreeRTOS WIFI tests 

(`iot_test_wifi`)

See [iot_test_wifi.c](https://github.com/aws/amazon-freertos/tree/master/libraries/abstractions/wifi/test)

Apparently the FreeRTOS WIFI Library includes about 30 tests to exercise
`iot_wifi.h`. [See the upstream
docs](https://docs.aws.amazon.com/freertos/latest/portingguide/afr-porting-wifi.html#porting-testing-wifi)

### test components

#### headers
| file                                                                 | which                                            | who             |
|----------------------------------------------------------------------|--------------------------------------------------|-----------------|
| `libraries/abstractions/wifi/include/iot_wifi.h`                     | The FreeRTOS WIFI interface                      | aip_supplicant  |
| `libraries/abstractions/wifi/test/iot_test_wifi.h`                   | 2nd STA configuration (test only)                | amazon-freertos |
| `tests/include/aws_clientcredential.h`                               | STA configuration                                | app             |
| `vendors/*/boards/*/aws_tests/config_files/aws_wifi_config.h`        | WiFi task priority, stack size, AP configuration | target          |
| `vendors/*/boards/*/aws_tests/config_files/aws_test_runner_config.h` | choose which tests to run                        | target          |
| `vendors/*/boards/*/aws_tests/config_files/aws_test_wifi_config.h`   | WiFi test task priority, etc.                    | target          |
| `libraries/3rdparty/unity/extras/fixture/src/unity_fixture.h`        | unity framework                                  | unity           |
| `tests/include/aws_test_{runner,tcp,utils}.h`                        | misc. aws_test helpers                           | amazon-freertos |

#### modules
| file                                               | which                                                         | who             |
|----------------------------------------------------|---------------------------------------------------------------|-----------------|
| `libraries/abstractions/wifi/test/iot_test_wifi.c` | FreeRTOS WIFI tests (`Full_WiFI`and `Quarantine_WiFi` suites) | amazon-freertos |
| `tests/common/aws_test_runner.c`                   | call `iot_test_wifi.c`                                        | amazon-freertos |


#### test execution

The target main.c will start the `TEST_RUNNER_RunTests_task` task when
directed. This task will in turn initialize unity and run the tests:

```c
void TEST_RUNNER_RunTests_task( void * pvParameters )
{
    /* Disable unused parameter warning. */
    ( void ) pvParameters;

    /* Initialize unity. */
    UnityFixture.Verbose = 1;
    UnityFixture.GroupFilter = 0;
    UnityFixture.NameFilter = testrunnerTEST_FILTER;
    UnityFixture.RepeatCount = 1;

    UNITY_BEGIN();

...

    RunTests();

...

    /* This task has finished.  FreeRTOS does not allow a task to run off the
     * end of its implementing function, so the task must be deleted. */
    vTaskDelete( NULL );
}

static void RunTests( void )
{
...
    #if ( testrunnerFULL_WIFI_ENABLED == 1 )
        RUN_TEST_GROUP( Full_WiFi );
        RUN_TEST_GROUP( Quarantine_WiFi );
    #endif
...
}
```


## hardware testbed system testing

The final loop is a couple Adapt HDPs in a testbed driven by [aip_wtf](http://git.campbell.adapt-ip.com/AIP/aip_wtf) and [`aip_test_wifi`](#freertos-wifi-tests)

## Ports

### FreeRTOS POSIX port

FreeRTOS can be ported as a Linux app. The FreeRTOS tasks are implemented as
pthreads, and ethernet communication happens over tun (or libpcap) interfaces. 

See [FreeRTOS linux simulator](https://www.freertos.org/FreeRTOS-simulator-for-Linux.html) and the [FreeRTOS+TCP examples on windows](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/examples_FreeRTOS_simulator.html#static-dynamic) page.

- hack up a wpa_supplicant virtual wifi driver which just writes to libpcap

| pros                                                                       | cons                             |
|----------------------------------------------------------------------------|----------------------------------|
| can exercise (almost) the entire phobos stack north of the application API | need to write tests from scratch |
| can execute the `aws_test_wifi`                                            |                                  |
| can (probably) get a capture of network traffic                            | can strip the 802.3 header?      |

### FreeRTOS qemu port

- virtualize FreeRTOS (phobos-hdp) in qemu
- leverage aip sw_phy to emulate hardware
- implement lower data path as ethernet driver

| pros                         | cons        |
|------------------------------|-------------|
| test the entire phobos stack | lot of work |
| test target architecture     |             |
| can execute `aws_test_wifi`  |             |

