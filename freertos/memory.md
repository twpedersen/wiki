# static memory usage
## M20.1
| module                | text + .bss | total  |
|-----------------------|-------------|--------|
| wpa_supplicant        | 139991      | 139991 |
| wpa_supplicant crypto | 21203       | 161194 |
| FreeRTOS+TCP          | 23298       | 184492 |
| AIP driver            | 15622       | 200114 |

| module                       | text + .bss | total         |
|------------------------------|-------------|---------------|
| AIP debug + CLI              | 17,201      | 218083 (214k) |
| AIP radio driver             | 48,129      | 266932 (261k) |
| Phobos tests (iot_test_wifi) | 18,823      | 285755 (279k) |
| Phobos CLI                   | 12,123      | 297878 (286k) |
| Apps (iperf, ping, ipconfig) | 9,713       | 303591 (296k) |
| stdout debugging             | 5,353       | 308944 (302k) |

min. AP build (make prod PHOBOS_MIN_BUILD=1 PHOBOS_ENABLE_AP=1)
== 324705
