---
title: datapath
description: 
published: true
date: 2020-12-01T19:32:17.283Z
tags: 
editor: markdown
dateCreated: 2020-11-30T22:27:12.582Z
---

<div drawio-diagram="32"><img src="http://docs.campbell.adapt-ip.com/uploads/images/drawio/2021-02/drawing-3-1614016138.png"></div>

---

<div drawio-diagram="31"><img src="http://docs.campbell.adapt-ip.com/uploads/images/drawio/2020-12/drawing-3-1607736199.png"></div>

## Phobos

### Directory layout

The phobos project has several top level folders:

- **aip_supplicant**: Adapt-IP hostap fork which contains the wireless MAC and driver
- **FreeRTOS**: FreeRTOS version `202012.00-LTS` with some small fixes on top
- **phobos**: Phobos application directory. Contains test application, CLI, and misc. utilities
- **target**: target specific Makefiles and support code
- **test**: Unity test framework and Amazon IOT WIFI integration tests

### Interface

The Phobos interface is specified in `aip_wifi.h` (which itself includes `iot_wifi.h`, the existing [FreeRTOS Wi-FI API](https://docs.aws.amazon.com/freertos/latest/userguide/freertos-wifi.html)).

`aip_wifi.h` exposes  `aip_wifi_cfg()`, which lets the user specify additional configuration prior to `WIFI_On()`:

```c
struct wifi_on_opts {
	uint8_t ip_addr[IP_ADDR_SIZE];
	uint8_t netmask[IP_ADDR_SIZE];
	uint8_t gateway[IP_ADDR_SIZE];
	uint8_t dns[IP_ADDR_SIZE];
	int wifi_debug_level;
	int passive_scan;
	int rts_threshold;
	int bss_color;
	int ap_max_inactivity;
};

extern struct wifi_on_opts wifi_opts;

/* configure initial wifi options with a struct wifi_on_opts
 * An initial configuration should be supplied before calling WIFI_On()
 */
void aip_wifi_cfg(struct wifi_on_opts *opts);
```

The default WIFI configuration is specified as `DEFAULT_WIFI_OPTS` in
`phobos/config/phobos_config.h`.

| option            | description                                                                                                                                 |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
| iface             | Interface name, only used by the `posix` target                                                                                             |
| ip_addr           | IP address if DHCP fails                                                                                                                    |
| netmask           | IP netmask if DHCP fails                                                                                                                    |
| gateway           | IP gateway if DHCP fails                                                                                                                    |
| dns               | IP DNS if DHCP fails                                                                                                                        |
| wifi_debug_level  | `aip_supplicant` debug level                                                                                                                |
| passive_scan      | select passive instead of active scan. Applies to STA only                                                                                  |
| rts_threshold     | dot11RTSThreshold. Set to 0 to disable RTS                                                                                                  |
| bss_color         | The `BSS_COLOR`. Configurable on AP only                                                                                                    |
| ap_max_inactivity | Max. STA idle time after which AP will send a QoS NULL to check if the STA is still present. Units are in seconds. Only configurable on AP. |


### Configuration

#### compile time

Configuration of the various Phobos modules is done via header files.

| header                                 | configures                         |
|----------------------------------------|------------------------------------|
| `phobos/config/phobos_config.h`        | IP networking and task priorities  |
| `phobos/config/aws_wifi_config.h`      | WiFi                               |

#### runtime

The following options are available on both the `posix` and `hdp` targets. To
specify options for the latter, append them to the u-boot `bootelf` command. 

```bash
Usage: ./target/posix/build/phobos [-i iface] [-t] [-d <debug level>]
-a:                     become AWS test AP
-i <iface>:             which interface to use
-s:                     run ASWS test STA
-t:                     run AWS IOT tests
-d <debug_level>:       WIFI debug level where <debug_level> is one of:
                        0: MSG_EXCESSIVE
                        1: MSG_MSGDUMP
                        2: MSG_DEBUG
                        3: MSG_INFO (default)
                        4: MSG_WARNING
                        5:MSG_ERROR
```

## FreeRTOS+TCP/IP

**FreeRTOS+TCP** provides the TCP/IP/ARP implementation and lives in `FreeRTOS/FreeRTOS-Plus/Source/FreeRTOS-Plus-TCP`.

The [application interface](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/FreeRTOS_TCP_API_Functions.html) is similar to the Berkeley Sockets API.

**aip_supplicant** implements a FreeRTOS+TCP driver, and will automatically initialize FreeRTOS+TCP when the WiFi link comes up.

### buffer management (allocation)

See the [FreeRTOS+TCP Buffer Management](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/Embedded_Ethernet_Buffer_Management.html) page.

| scheme               | allocation        | RAM usage          | complexity |
|----------------------|-------------------|--------------------|------------|
| BufferAllocation_1.c | static by driver  | same size buffers  | high       |
| BufferAllocation_2.c | dynamic from heap | right size buffers | low        |

Based on the table above (and the recommendation of the [FreeRTOS+TCP README](https://github.com/FreeRTOS/FreeRTOS-Plus-TCP#note)), Phobos is using the `BufferAllocation_2.c` scheme.

### 802.11 headroom

FreeRTOS+TCP sends (and receives) 802.3 frames. These have a 14 byte header
which must be translated into an 802.11 header with roughly 24 bytes on TX.
This means the `NetworkBufferDescriptor_t` needs some additional headroom which is requested by setting [ipconfigBUFFER_PADDING](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/TCP_IP_Configuration.html#ipconfigBUFFER_PADDING).

## aip_supplicant

See the [official developer documentation](https://w1.fi/wpa_supplicant/devel/) for sections not covered by this document.

The [porting page](https://w1.fi/wpa_supplicant/devel/porting.html) is especially interesting.

### Directory Structure

#### `freertos/`

FreeRTOS and external interfaces go into the top level `freertos/` directory.

| file                 | description                                  |
|----------------------|----------------------------------------------|
| `aip_wifi.h`         | Phobos WIFI interface                        |
| `freertos_tcp.c`     | FreeRTOS+TCP driver interface                |
| `iot_wifi.{c,h}`     | FreeRTOS WIFI interface                      |
| `wpa_freertos.{c,h}` | glue between `iot_wifi` and `wpa_supplicant` |
| `wpa_cli.c`          | `wpa_cli` implementation                     |


#### `src/mac/`

The `wpa_mac` and netbuf implementation lives in `src/mac/`.

| file                        | description                                     |
|-----------------------------|-------------------------------------------------|
| `thinmac.h`                 | thinmac interface, called by `driver_thinmac.c` |
| `mac.h`                     | TX/RX control and status info                   |
| `netbuf.{c,h}`              | netbuf implementation                           |
| `rc.{c,h}`                  | rate control                                    |
| `rx.c, tx.c, mac.c, scan.c` | main data path and `wpa_mac` implementation     |
| `drv.h`                     | thinmac driver glue                             |


#### `/src/drivers`

| file                           | description                                                     |
|--------------------------------|-----------------------------------------------------------------|
| `src/drivers/driver_thinmac.h` | thinmac driver interface. Defines `wpa_thinmac_driver_ops`      |
| `src/drivers/driver_thinmac.c` | implements `wpa_driver_ops` by calling into `src/mac/thinmac.h` |

#### misc

| file                                   | description                                |
|----------------------------------------|--------------------------------------------|
| `src/utils/os_freertos.c`              | FreeRTOS specific OS abstraction layer     |
| `src/utils/eloop_freertos.c`           | FreeRTOS eloop (event loop) implementation |
| `wpa_supplicant/os_notify.{c,h}`       | lightweight WIFI event module              |


### threading

wpa_supplicant is single threaded. After init, Events (and IPC messages) are
handled by the `eloop_run()` loop in `eloop.c`. This means wpa_supplicant
generally doesn't have to worry about locking.

When we want to call into wpa_supplicant from FreeRTOS there is a problem: a
lack of good IPC. For example in a POSIX OS you can register an event listener
(IPC handler) with `eloop_register_read_sock()`, which will add the unix domain
socket to the listen set. The message socket is bidirectional, so a response
can be sent back to the caller through the same socket. FreeRTOS only has
message queues which are unidirectional.

So, for FreeRTOS we take a global mutex in `iot_wifi.c` and call into
wpa_supplicant directly. `eloop_freertos.c` then also takes this mutex before
handling any events. This ensures wpa_supplicant remains serialized without any
major changes.

The datapath also needs to ensure mutual exclusion with the main wpa_supplicant
loop, so needs to either take the global mutex (`wpa_freertos_lock()`), or
schedule on the main thread (`eloop_trigger_event()`) before calling into
wpa_supplicant.

### Initialization

`WIFI_On()` spawns a FreeRTOS task `wpa_s` which initializes a single global
`wpa_supplicant` context and otherwise goes through the usual wpa_supplicant
init.

Once wpa_supplicant has been successfully initialized, `WIFI_On()` will
initialize FreeRTOS+TCP by calling `freertos_tcp_init()`. If a statis IP
configuration is supplied, it will be applied at this time.

### FreeRTOS+TCP

wpa_supplicant implements a **FreeRTOS+TCP** driver in `freertos/freertos_tcp.c`. This
file implements `xNetworkInterfaceOutput()` for TX and calls
`xSendEventStructToIPTask()` on RX.

`xNetworkInterfaceInitialise()` is stubbed out for now.

`vIPNetworkUpCalls()` is called when AP is brought up or STA successfully
associates. `FreeRTOS_NetworkDown()` is called when the WiFi link goes down.

### Interface

`iot_wifi.h` specifies a common [FreeRTOS Wi-FI API](https://docs.aws.amazon.com/freertos/latest/userguide/freertos-wifi.html).

aip_supplicant implements the following interface:

| Function           | Description                                              |
|--------------------|----------------------------------------------------------|
| WIFI_On            | Turns on Wi-Fi module and initializes the drivers.       |
| WIFI_Off           | Turns off Wi-Fi module and deinitializes the drivers.    |
| WIFI_Reset         | Reset the Wi-Fi module, currently just does a disconnect |
| WIFI_SetMode       | configure AP or STA mode                                 |
| WIFI_GetMode       | return configured AP or STA mode                         |
| WIFI_ConnectAP     | Connects to a Wi-Fi access point (AP).                   |
| WIFI_Disconnect    | Disconnects from an AP.                                  |
| WIFI_StartAP       | bring up a configured AP                                 |
| WIFI_StopAP        | bring down a running AP                                  |
| WIFI_ConfigureAP   | configure an AP                                          |
| WIFI_IsConnected   | check WiFi link state                                    |
| WIFI_RegisterEvent | register WiFi event callback                             |
| WIFI_Scan          | Performs a Wi-Fi network scan.                           |
| WIFI_GetIP         | Retrieves the Wi-Fi interface’s IP address.              |
| WIFI_GetIPInfo     | Retrieves the Wi-Fi interface’s IP address.              |
| WIFI_GetMAC        | Retrieves the Wi-Fi interface’s MAC address.             |
| WIFI_GetHostIP     | Retrieves the host IP address from a hostname using DNS. |
| WIFI_Ping          | Trigger ICMP Echo Request                                |


### events

#### os_notify
Various events (associated, scan complete, etc.) will be emitted by the
wpa_supplicant MAC.  The existing implementation is mostly through `wpa_msg()`
which adds about 35k to the code size, so we've added a lightweight
notification system `os_notify`.

The `os_notify` event looks like:

```c
struct wpas_f_event {
    enum wpas_notify_event event;
    int status;
    void *data;
};
```

`freertos/iot_wifi.c` has a dedicated listener task to dispatch `WIFIEvents` to
the Phobos application.

eg. `WIFI_On()` also uses this to determine whether wpa_supplicant init is
complete.

#### eloop.c

Since FreeRTOS does not provide sufficiently featureful IPC (ie. Unix domain
sockets) which provide bidirectional communication, we must simplify the eloop
implementation.

`eloop_freertos.c` provides:

1. timers using the FreeRTOS timer infrastructure.
2. event handling through a single message queue.

Events are queued using the simple `eloop_trigger_event()`:
```c
int eloop_trigger_event(eloop_event_handler handler,
                        void *eloop_data, void *user_data)
```



### control interface

In addition to the regular log output, wpa_supplicant also has a control
interface. This is useful for configuring or getting status information.

The `freertos/wpa_cli.c` provides a simple `wpa_cli` with the following
interface:

```
usage: wpa_cli <cmd>:
where <cmd> is one of:

disconnect:               disassociate
get_network <param>:      get network parameter
help:                     this message
list_networks:            list configured networks
log_level:                display the current log level
log_level <level>:        set the current log level which should be one of:
                          [EXCESSIVE, MSGDUMP, DEBUG, INFO, WARNING, ERROR]
reassociate:              reassociate to current network
sta <mac addr>:           dump info for <mac addr> (currently AP only)
scan:                     do a scan
scan_results:             dump scan_results
set_network <param>:      set network parameter
status:                   wifi status
```

### data path

The wpa_supplicant data path is implemented in `src/mac/{tx,rx}.c` and
provides:

- mapping a RX/TX frame to the correct STA. 
- 802.11 encap and decap
- rate control
- crypto

The data frame is referred to by a TX/RX descriptor to describe metadata such
as desired TX rate, retry count, key, etc. This descriptor lives in the
`netbuf->cb` area.

#### TX

1. **FreeRTOS+TCP** calls `xNetworkInterfaceOutput()` in `freertos/txrx.c`.
2. takes the wpa_supplicant mutex and calls into `wpa_tx`.
3. `wpa_tx` does: 
    - 802.11 encapsulation (inluding sequence number assignment)
    - rate control
    - encryption
    - etc.
4. finally, `wpa_tx` calls `wpa_driver_ops->tx` for aip.

#### RX

1. the driver gets an interrupt, and eventually calls `wpa_rx` from `eloop_trigger_event()` context.
2. `wpa_rx` does:
    - deduplication
    - decryption
    - 802.11 decapsulation
3. finally, `wpa_rx` calls [xSendEventStructToIPTask()](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/API/xSendEventStructToIPTask.html) 

#### zero copy RX

Zero copy RX means the driver preallocates a buffer, then passes that buffer to **FreeRTOS+TCP** without doing a `memcpy()` of the payload.

See the [upstream documentation](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/Embedded_Ethernet_Porting.html#network_buffers_and_Ethernet_buffers) for more information.

#### data path locking

The data path should be serialized with respect to the rest of wpa_supplicant.
Either through taking the global FreeRTOS mutex directly or running from the
event queue.

#### rate control

Rate control lives in `src/mac/rc.c`. It exposes `src/mac/rc.h`.

##### structures

`struct sta_info` has supported rates defined in **TODO** which are used by
rate control to initialize the rate table.

`struct wpa_rc_info` is embedded in `struct wpa_tx_info` , and describes the
rate control table.

```c
struct wpa_tx_info {
    ...
#define WPA_MAX_RATES 3
    struct wpa_rc_info rates[WPA_MAX_RATES];
    ...
};
```

`struct wpa_rc_info` is used for both controlling the TX rate and number of
attempts, and updated by the driver after a completed transmission attempt to
provide rate feedback.

```c
struct wpa_rc_info {
    struct wpa_rate rate;
    /* total transmissions at this rate */
    u8 count;

    /* if this is 1, rate was acked after @count transmissions */
    u8 acked;
};
```
```c
struct wpa_rate {
    u8 mcs;
    u8 bw;
#define WPA_RATE_FLAG_SGI       BIT(0)
#define WPA_RATE_FLAG_SAMPLE    BIT(1)
#define WPA_RATE_NOACK          BIT(2)
    u32 flags;
};
```

##### functions

The rate control interface is defined in `src/mac/rc.h`:
```c
/**
 * @brief Obtain a TX rateset for given netbuf
 *
 * Fill in the TX rateset pointed to by @rates (of length WPA_MAX_RATES),  based
 * on the current rate table for @sta. The ratesset is initialized to 0 and
 * unused rateset entries will have a count of 0.
 *
 * @param sta The STA info
 * @param rates The rateset to fill in.
 */
void wpa_rc_get_rates(struct sta_info *sta, struct wpa_rc_info *rates);

/**
 * @brief Provide feedback to the rate control algorithm.
 *
 * Rate attempts are provide to rate control in the array pointed to by @rates
 * (of length WPA_MAX_RATES). The driver may indicate success of the first rate
 * by filling in the @rates at the corresponding transmission index, eg:
 *
 * ```
 *	rates[0].count = 2;
 *	rates[0].status.txed = 1;
 *	rates[0].status.acked = 0;
 *
 *	rates[1].count = 1;
 *	rates[1].status.txed = 1;
 *	rates[2].status.acked = 1;
 * ```
 *
 * @param sta The STA this update is for
 * @param rates The rate table containing TX status
 */
void wpa_rc_report_status(struct sta_info *sta, struct wpa_rc_info *rates);

/**
 * @brief Initialize rate control structs (rate table) for given @sta.
 *
 * @param sta The sta_info to initialize
 * @param s1g_cap The S1G capabilities negotiated for this STA
 */
void wpa_rc_init_sta(struct sta_info *sta,
		     struct ieee80211_s1g_capabilities *s1g_cap);


/**
 * @brief Initialize global rate control structures.
 *
 * @param wpa_s Context.
 */
int wpa_rc_init(struct wpa_supplicant *wpa_s);

/**
 * @brief Deinitialize global rate control structures.
 *
 * @param wpa_s Context.
 */
int wpa_rc_deinit(struct wpa_supplicant *wpa_s);

/**
 * @brief Dump the rate table contained in @sta to @buf.
 *
 * @return The number of bytes written.
 * @param sta the sta_info
 * @param buf The buffer to write to
 * @param buflen The length of @buf
 */
int wpa_rc_dump_info(struct sta_info *sta, char *buf, size_t buflen);
```

#### crypto

**TBD**

### thinmac

There are 2 main wifi driver models:

- **fullmac**: driver implements MAC functions like scanning, assoc, etc.
- **thinmac**: driver expects upper layer to implement MAC function

wpa_supplicant expects a **fullmac** driver, hence driver operations like `.scan()` etc.

We implement a library `wpa_mac` which fills in missing MAC functionality
(operations) for **thinmac** drivers.

Consider the following ops:

```c
const struct wpa_driver_ops wpa_driver_thinmac_ops = {
	.name = "thinmac",
	.desc = "thinmac driver",
...
	.scan2 = wpa_thinmac_scan,
```

`wpa_mac_scan` implements a scanning state machine normally provided by the
driver, eventually calling the `wpa_thinmac_driver_ops` `.tx()` callback.

#### driver requirements

To implement a thinmac driver.

- create a new file `src/driver/driver_mydriver.c`
- `#include "driver_thinmac.h"`
- create a `struct wpa_thinmac_driver_ops` and declare it as extern in `src/drivers/driver.h`
- add your `wpa_thinmac_driver_ops` to the thinmac ops array in `src/drivers/driver_thinmac.c`
- handle `CONFIG_MYDRIVER` in `src/drivers/drivers.mak` to include your object and set `NEED_THINMAC=1`

## drivers
### Posix FreeRTOS driver

[![phobos_testing_posix.jpg](http://docs.campbell.adapt-ip.com/uploads/images/gallery/2021-04/scaled-1680-/phobos-testing-posix.jpg)](http://docs.campbell.adapt-ip.com/uploads/images/gallery/2021-04/phobos-testing-posix.jpg)

`driver_fr_linux` or `frl`, or `FreeRTOS Linux` is a special wpa_supplicant
driver running on the FreeRTOS Posix port. It is useful for testing everything
above the driver layer without actual hardware.

The FreeRTOS Posix port implement FreeRTOS tasks as pthreads. `driver_fr_linux`
simulates 802.11 transmissions by sending and receiving through raw sockets
connected to a linux veth pair.

The driver is implemented in `src/drivers/driver_frlinux.c`

### interrupts

TBD

## testing

Phobos leverages the Amazon IOT WIFI tests which are based on Unity and distributed as [part of Amazon-FreeRTOS](https://github.com/sifive/Amazon-FreeRTOS/blob/master/libraries/abstractions/wifi/test/iot_test_wifi.c)

The tests work on both the Posix and HDP port, and can be triggered by passing
the `-t` options to phobos (either as executable arguments or on the u-boot
cmdline).

```
TEST(Full_WiFi, AFQP_WIFI_Scan) PASS
TEST(Full_WiFi, AFQP_WIFI_GetMode_NullParameters) PASS
TEST(Full_WiFi, AFQP_WIFI_GetIP_NullParameters) PASS
TEST(Full_WiFi, AFQP_WIFI_GetMAC_NullParameters) PASS
TEST(Full_WiFi, AFQP_WIFI_GetHostIP_NullParameters) PASS
TEST(Full_WiFi, AFQP_WIFI_Scan_NullParameters) PASS
TEST(Full_WiFi, AFQP_WIFI_Ping_NullParameters) PASS
TEST(Full_WiFi, AFQP_WIFI_ConnectAP_NullParameters) PASS
TEST(Full_WiFi, AFQP_WIFI_SetMode_InvalidMode) PASS
TEST(Full_WiFi, AFQP_WIFI_GetHostIP_InvalidDomainName) PASS
TEST(Full_WiFi, AFQP_WIFI_GetHostIP_DomainNameLengthExceeded) PASS
TEST(Full_WiFi, AFQP_WIFI_NetworkDelete_DeleteNonExistingNetwork) PASS
TEST(Full_WiFi, AFQP_WIFI_ConnectAP_InvalidSSID) PASS
TEST(Full_WiFi, AFQP_WIFI_ConnectAP_InvalidPassword) PASS
TEST(Full_WiFi, AFQP_WIFI_ConnectAP_MaxSSIDLengthExceeded) PASS
TEST(Full_WiFi, AFQP_WIFI_ConnectAP_MaxPasswordLengthExceeded) PASS
TEST(Quarantine_WiFi, AFQP_WiFiOnOff) PASS
TEST(Quarantine_WiFi, AFQP_WiFiMode) PASS
TEST(Quarantine_WiFi, AFQP_WiFiConnectionLoop) PASS
TEST(Quarantine_WiFi, AFQP_WiFiPowerManagementMode) PASS
TEST(Quarantine_WiFi, AFQP_WiFiGetIP) PASS
TEST(Quarantine_WiFi, AFQP_WiFiGetMAC) PASS
TEST(Quarantine_WiFi, AFQP_WiFiScan) PASS
TEST(Quarantine_WiFi, AFQP_WiFiReset) PASS
TEST(Quarantine_WiFi, AFQP_WiFiPing) PASS
TEST(Quarantine_WiFi, AFQP_WiFiIsConnected) PASS
TEST(Quarantine_WiFi, AFQP_WiFiOnOffLoop) PASS

-----------------------
27 Tests 0 Failures 0 Ignored
OK
```
