# FreeRTOS build

We should reorganize the current `phobos-hdp` layout to support:

1. easy integration with upstream FreeRTOS and FreeRTOS-Plus modules
2. build the same components for different targets without a make clean

Currently, `phobos-hdp` has most things squashed into a single `src/` directoy:

```
├── AIP_HDP_hw_platform			# FPGA stuff
├── doc
├── phobos
│   └── src
│       ├── aip_supplicant
│       ├── config			# target specific FreeRTOSConfig. is here
│       ├── FreeRTOS-Plus-CLI		# FreeRTOS-Plus-CLI component
│       ├── FreeRTOS_Source		# FreeRTOS kernel source
│       ├── Full_Demo			# related to FreeRTOS-Plus-CLI
└── phobos_bsp				# HDP BSP
```

The (compressed) FreeRTOS directory structure looks like:

```
├── FreeRTOS
│   ├── Demo
│   │   ├── CORTEX_A9_Zynq_ZC702	# HDP specific init and app
...
│   │   ├── Posix_GCC			# Posix GCC helpers and demo
...
│   │   └── Xilinx_FreeRTOS_BSP		# HDP BSP
│   ├── License
│   ├── Source				# FreeRTOS kernel source
│   └── Test
├── FreeRTOS-Plus
│   ├── Source
...
│   │   ├── FreeRTOS-Plus-CLI		# FreeRTOS-Plus libraries linked by projects in Demo/
│   │   ├── FreeRTOS-Plus-TCP
│   │   ├── FreeRTOS-Plus-Trace
...
│   │   └── WolfSSL
│   ├── Test
│   │   ├── CMock
│   │   └── FreeRTOS-Plus-TCP
│   └── ThirdParty
│       └── mbedtls
└── tools
    └── cmock
```

What we really want is probably take what we have and rebase on the upstream
**FreeRTOS.git** and separate out the target specific stuff. The target
specific dirs control their own make process and will just compile / link in
objects as needed.

```
.
├── AIP_HDP_hw_platform
├── aip_supplicant
├── FreeRTOS
│   ├── Source
│   │   └── portable
│   │       ├── GCC
│   │       │   └── ARM_CA9
│   │       └── ThirdParty
│   │           └── GCC
│   │               └── Posix
│   └── target				# target specific goes here
│       ├── hdp
│       ├── hdp_bsp
│       └── posix
├── FreeRTOS-Plus
│   └── Source
│       ├── FreeRTOS-PLUS-CLI
│       ├── FreeRTOS-PLUS-TCP
│       ├── FreeRTOS-PLUS-Trace
│       └── WolfSSL
└── tools
    └── cmock
```

Then build is:

	make -C FreeRTOS/target/hdp
	make -C FreeRTOS/target/posix
	make -C FreeRTOS/target/posix test

etc. without having to do a make clean in between.

Also we need to move the build files (object output) into the target specific
`FreeRTOS/target/<target>/build`. Hard to do for `aip_supplicant` since it is
self contained. Could just rsync from the top level `aip_supplicant` into
`<target>/build` before making.

## final build layout

```
.
├── aip_supplicant		# submodule pointing to our hostap.git
├── doc
├── FreeRTOS			# submodule pointing upstream
│   ├── FreeRTOS
│   ├── FreeRTOS-Plus
│   └── tools
└── target
    └── hdp			# phobos for HDP specific files
        ├── bsp
        ├── build		# build output goes here
        ├── config
        ├── Full_Demo		# CLI specific thing
        └── hw			# hardware FPGA image
```

