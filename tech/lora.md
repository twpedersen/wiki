---
title: LoRa
description: 
published: true
date: 2021-06-30T16:28:20.088Z
tags: 
editor: markdown
dateCreated: 2021-06-29T18:03:33.247Z
---

# LoRa
![2021-06-29-142255_930x456_scrot.png](/2021-06-29-142255_930x456_scrot.png)
[Link Labs white paper](/lpwan-brochure-interactive.pdf)
[lorawan_link_layer_specification_v1.0.4.pdf](/lorawan_link_layer_specification_v1.0.4.pdf)
[stackforce semtech LoRaMac documentation](https://stackforce.github.io/LoRaMac-doc/LoRaMac-doc-v4.5.2/index.html)
[LoRaWan packet decoder](https://lorawan-packet-decoder-0ta6puiniaut.runkit.sh/?))

## MACs
- [LoRaMac-node](https://github.com/Lora-net/LoRaMac-node)
  - SemTech stack
  - used by FreeRTOS LoRaMac implementation
  
- [basicmac](https://basicmac.io/)
  - SemTech bought company supporting this stack, [now trying to EOL](https://github.com/lorabasics/basicmac) it in favor of `LoRaMac-node`.
  - one of the original developers is [continuing the effort](https://github.com/mkuyper/basicmac)
  - original open source LoRa MAC
  - has simulation environment (complete with python gateway implementation)
  - [basic documentation](https://basicmac.io/guide/gettingstarted.html)

## LoRa on FreeRTOS

[See official wiki page](https://www.freertos.org/lorawan/index.html)
[FreeRTOS LoRaWAN implementation](https://github.com/FreeRTOS/Lab-Project-FreeRTOS-LoRaWAN.git) (relies on LoRaMac-node)