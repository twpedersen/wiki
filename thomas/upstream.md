# S1G kernel
## features
- short beacon
- NDP probe request
- configure listen interval
- S1G BSS color
- S1G capabilities (STA + band info)
- S1G TIM
- upper TSF sync
- minstrel_s1g
- TWT
- WFA hacks
- S1G radiotap
- TLV vendor radiotap
- misc. fixes

## hostap freq offset

- bss->freq
