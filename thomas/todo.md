-  missing MacTimeStamp
- hardware cannot tell frame received on 1Mhz?
        qosdata,ppdu_format,
-
- crypto requirements slides
- review silex etc. info from farhad
- think about firmware testing
- fix udp_tput test (don't assoc?)
- start throughput testing on ribbon
- video progress

- order programmable attenuator and get that testing going
- review feature spreadsheet
- check channel verification logic has to be so complicated
- fix segfault on init error (no phobos1)
- update gitea crypto issue w/ straff notes
- SN for mgmt (auth, assoc, probe request/response) is random
- WTS tests:
    - HaL-4.2.17
    - SAE-5.2.7
    - TWT (5.2.28 + 4.2.32)
    - enable additional security tests
    - scope out implementing sniffer / injector

- hostap default EDCA params
- default TXOP for S1G:
    BK: 468
    BE: 468
    VI: 468
    VO: 468
- verify params make it to the driver
    - conf->tx_queue[] configures the driver

- check out and commit new wireshark
    - S1G is upstream now, rebase aip on upstream

- gather HW test results
- [ ] document (and draw) wpa_mac thinmac helper
    - update freertos architecture doc

- [ ] document size calculation methodology

- add os_notify to architecture drawing

- open stdin as raw in posix port

- fiddle with #  of netbuf descriptors
- uname -a output is wrong

- set NL80211_SCAN_FLAG_ACCEPT_BCAST_PROBE_RESP with NL80211_SCAN_FLAG_NDP_PROBE

# merged features

- vendor radiotap on TX
- NDP probe request
- NDP (probe request) RX
- radiotap NDP
- broadcast probe response RX
- short beacon (TSBTT)
- S1G agg setup, check AMPDU capabilities?
- RX radiotap: TLV, NDP, vendor
- branch off master before push -f (linux)
- tasks on monday
- early assoc failure

## rebase TODO

- filter out `test_*` tags when updating `latest` symlink
- no beacons on sw_phy
- nullfunc probe fails
- beacon miss triggers ?

## wifi_wpa.conf

```
ctrl_interface=/var/run/wpa_supplicant
ndp_probe=0
passive_scan=1
sae_groups=19 20 21
sae_pwe=1
network={
        disabled=1
        scan_ssid=1
        scan_freq=921.5 921 920.5
        ssid="HaL-5.2.1_340"
        key_mgmt=SAE
        s1g_sta_type=2
        listen_int=0
        ctl_resp_1mhz=0
        disable_s1g_ampdu=1
    proactive_key_caching=1
    ieee80211w=2
    pairwise=CCMP
    group=CCMP
    psk="12345678"
}
```

## 920MHz ??
```
[  217.692422] calling cfg80211_get_bss_channel band: 4
[  217.701907] got channel number from S1G primary ch: 38
[  217.707095] channel from bss: 920.0
[  217.711454] ------------[ cut here ]------------
[  217.716121] WARNING: CPU: 1 PID: 962 at backports/net/wireless/scan.c:1709 cfg80211_bss_update+0x78/0x7e4
[  217.725788] Modules linked in:
[  217.728882] CPU: 1 PID: 962 Comm: kworker/u4:4 Tainted: G        W         4.19.0-14741-g93161cfcdd20 #9
[  217.738367] Hardware name: Xilinx Zynq Platform
[  217.742914] Workqueue: phy0 ieee80211_iface_work
[  217.747573] [<c0114a9c>] (unwind_backtrace) from [<c010f50c>] (show_stack+0x20/0x24)
[  217.755333] [<c010f50c>] (show_stack) from [<c0de2bac>] (dump_stack+0xac/0xd8)
[  217.762578] [<c0de2bac>] (dump_stack) from [<c0127ab0>] (__warn.part.0+0xc0/0xf0)
[  217.770079] [<c0127ab0>] (__warn.part.0) from [<c0127b34>] (warn_slowpath_null+0x54/0x5c)
[  217.778275] [<c0127b34>] (warn_slowpath_null) from [<c0c9d938>] (cfg80211_bss_update+0x78/0x7e4)
[  217.787073] [<c0c9d938>] (cfg80211_bss_update) from [<c0c9f434>] (cfg80211_inform_single_bss_frame_data+0x3dc/0x734)
[  217.797610] [<c0c9f434>] (cfg80211_inform_single_bss_frame_data) from [<c0c9f7d8>] (cfg80211_inform_bss_frame_data+0x4c/0x48c)                                                                                                                         
[  217.809015] [<c0c9f7d8>] (cfg80211_inform_bss_frame_data) from [<c0d17a40>] (ieee80211_bss_info_update+0x19c/0x424)
[  217.819461] [<c0d17a40>] (ieee80211_bss_info_update) from [<c0d82c34>] (ieee80211_rx_bss_info+0x88/0xd0)
[  217.828957] [<c0d82c34>] (ieee80211_rx_bss_info) from [<c0d8d894>] (ieee80211_sta_rx_queued_mgmt+0x458/0x1410)
[  217.838972] [<c0d8d894>] (ieee80211_sta_rx_queued_mgmt) from [<c0d298e0>] (ieee80211_iface_work+0x2d4/0x410)
[  217.848813] [<c0d298e0>] (ieee80211_iface_work) from [<c014be5c>] (process_one_work+0x2b4/0x870)
[  217.857612] [<c014be5c>] (process_one_work) from [<c014c868>] (worker_thread+0x68/0x578)
[  217.865713] [<c014c868>] (worker_thread) from [<c0152750>] (kthread+0x154/0x184)
[  217.873126] [<c0152750>] (kthread) from [<c01010b4>] (ret_from_fork+0x14/0x20)
[  217.880356] Exception stack(0xedf2bfb0 to 0xedf2bff8)
[  217.885411] bfa0:                                     00000000 00000000 00000000 00000000
[  217.893598] bfc0: 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000
[  217.901781] bfe0: 00000000 00000000 00000000 00000000 00000013 00000000
[  217.908472] irq event stamp: 36842
[  217.911936] hardirqs last  enabled at (36852): [<c018f74c>] console_unlock+0x490/0x690
[  217.919865] hardirqs last disabled at (36859): [<c018f374>] console_unlock+0xb8/0x690
[  217.927767] softirqs last  enabled at (36794): [<c010268c>] __do_softirq+0x37c/0x5e8
[  217.935575] softirqs last disabled at (36806): [<c0c9d904>] cfg80211_bss_update+0x44/0x7e4
[  217.943908] ---[ end trace c5085d3ba8afd0ef ]---
[  217.948537] create new bss
[  217.951766] channel from bss final: 920.0
```
## WTS
- [initial run](http://test.campbell.adapt-ip.com/wts/wts_pass/2021-09-28T13%3A21%3A57-07%3A00/?C=M&O=D)

# integrated backports

## this
output/build/linux-custom/backports/net/mac80211/Kconfig
	select BACKPORT_BPAUTO_CRYPTO_LIB_ARC4

## that
    remove `option modules` from output/build/linux-custom/backports/compat/Kconfig 
# DONE

## 6/04/21

- aip started beaconing
- aip tx_safe = 0
- basic beacon miss logic in wpa_s
- ipconfig, heap stats

## 5/07/21

- fix aip interrupts
- zero copy tx (buffer fragment / scatter gather still TODO)
- track STA TX latency
- rate control interface
- simulate TX duration in SW driver
- added ping summary

### TODO

- bugs
- long term ping + iperf test
- documentation
- map DSCP to AC

## 3/05/21

- implement various iot_wifi utilities like WIFI_GetMAC() etc.
- aip driver can come up as AP
- warnings are gone
- fix uboot boot, now accepts optarg flags
- phobos refactor to be more independent of target build

## 2/26/21

- stub out wpa_supplicant
- warnings
- sequence number assignment
- beaconing
- fixup 802.11 encap/decap
- factor out phobos

## 2/19/21

- fix simulator lockup
- S1G channels
- S1G association
- active scanning (2MHz channels)
- return results to WIFI_Scan()
