# OS variants

We need to test all possible combinations of (os, role) between two boards.
These are:

| ap_os  | sta_os |
|--------|--------|
| phobos | phobos |
| phobos | linux  |
| linux  | phobos |
| linux  | linux  |

consider `variants.yml`:
```
os: !mux
    phobos:
        os: phobos
    linux:
        os: linux
role: !mux
  ap:
    role: ap
  sta:
    role: sta
```

`$ avocado variants -m variants.yml --summary 2`:
```
Multiplex tree representation:
 ┗━━ run
      ┣━━ os
      ┃    ╠══ phobos
      ┃    ║     → os: phobos
      ┃    ╚══ linux
      ┃          → os: linux
      ┗━━ role
           ╠══ ap
           ║     → role: ap
           ╚══ sta
                 → role: sta

Multiplex variants (4):
Variant run-os-phobos-role-ap-551f:    /run/os/phobos, /run/role/ap
Variant run-os-phobos-role-sta-1ba0:    /run/os/phobos, /run/role/sta
Variant run-os-linux-role-ap-a3bb:    /run/os/linux, /run/role/ap
Variant run-os-linux-role-sta-b7b3:    /run/os/linux, /run/role/sta
```
