- LMAC RTOS
    - see [wiki app note](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841653/XAPP1078+Latest+Information)
    - [original app note](https://www.xilinx.com/support/documentation/application_notes/xapp1078-amp-linux-bare-metal.pdf)

# openamp documentation
    - [opemamp app note](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_3/ug1186-zynq-openamp-gsg.pdf)
    - [openamp wiki](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/1896251596/OpenAMP+2021.1)
    - [openamp whitepaper](https://www.openampproject.org/docs/whitepapers/Introduction_to_OpenAMPlib_v1.1a.pdf)

# openamp support

- `BR2_PACKAGE_LIBSYSFS`
- libmetal
- elf payload
- enable remoteproc linux: `CONFIG_REMOTEPROC=y` and `CONFIG_ZYNQ_REMOTEPROC=y`
- libmetal abstraction layer for firmware

# devicetree changes
see `Documentation/devicetree/bindings/remoteproc/zynq_remoteproc.txt`
``` 
reserved-memory {
		#address-cells = <1>;

		#size-cells = <1>;
		ranges;
		rproc_0_reserved: rproc@3e000000 {
			  no-map;
			  reg =  <0x3e000000 0x01000000>;
		  };
	};

	amba {
		elf_ddr_0: ddr@0 {
			compatible = "mmio-sram";
			reg = <0x3e000000 0x400000>;
		};
	};

	remoteproc0: remoteproc@0 {
		compatible = "xlnx,zynq_remoteproc";
		firmware = "firmware";
		vring0 = <15>;

		vring1 = <14>;
		srams = <&elf_ddr_0>;
	};
```

# load firmware
```
echo phobos > /sys/class/remoteproc/remoteproc0/firmware
echo start > /sys/class/remoteproc/remoteproc0/state
```

# petalinux
https://www.xilinx.com/support/documentation/sw_manuals/xilinx2019_1/ug1186-zynq-openamp-gsg.pdf

# test

onto target:
- grab https://github.com/Xilinx/meta-openamp/tree/master/recipes-openamp/rpmsg-examples/rpmsg-echo-test
- copy kernel headers:
  scp -r ./output/build/linux-headers-aip_2019_R2-1/usr/include/ root@10.10.10.244:/usr/ 
- make
