# buildroot 2021.08.x rebase

- [x] wts_pass
    - [37/39](http://test.campbell.adapt-ip.com/wts/wts_pass/2021.08.x_rebase_test/2021-09-14T11%3A30%3A07-07%3A00/?C=M&O=D)
    - [pass on second run](http://test.campbell.adapt-ip.com/wts/test_manifest/2021.08.x_rebase_test/2021-09-14T14%3A06%3A51-07%3A00/?C=M&O=D)
- [x] tshark
- [x] gcc
- [x] git
- [x] `make clean && make` in:
    - [x] loci
    - [x] radio_fir
    - [x] radio_raw
    - [x] wasabi
