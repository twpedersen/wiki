# Adapt-IP Software Team Future

## build
- improve buildroot tagging (optimize out noop tags)

## regression testing
- increase reliability of WTS tests
- add 3rd party sniffer (aip board?)
- start running (3500+) hostap hwsim tests on hostap or kernel changes

## OS
- create openwrt based aip_os
- resolve linux OTA upgrades (openwrt?)

## open source
- upstream S1G changes
    - hostap
    - kernel
    - iw
- remain the leading open source S1G solution

## certification
### WFA??
### build aip WTS sniffer

## HW
- reimplement lower mac in software
- generate new hardware release
- fix PER issues
- tape out asap

## process

### documentation
- documentation process is shit
    - RFDs are used to propose, discuss, and approve architectural changes
        - see [oxide rfd](https://oxide.computer/blog/rfd-1-requests-for-discussion)
        - [example repo](https://github.com/joyent/rfd)

- need to standardize on a single wiki
- cannot be word document somewhere on a shared drive
- [wikijs](js.wiki) allows git-backed pages so you can document locally and publish notes

#### requirements
    - collaborative editing
    - version control
    - markdown format

### issue tracking
- gitea issues are sufficient for reporting and discussing bugs
- for project planning, an external tool (such as monday) is preferred
    - missing integration w/ gitea issues
- start planning in gitea?
- feature design / discussion is better in a wiki page or RFD

### communication

#### chat
- [zulip](zulip.com)(open source thread based chat) is interesting
- [matrix](matrix.org) allows federation similar to email, ie.
  `@thomas:adapt-ip.com` can talk to `@aaron:newracom.com`
    - pipe dream?

#### groupware
- email / calendar / contacts currently diy
- [list of self hosted groupware](https://github.com/awesome-selfhosted/awesome-selfhosted#groupware)

## IOT

### esp8266 / esp32

- [esp32](https://en.wikipedia.org/wiki/ESP32) wikipedia page
- [list of flashable devices](https://templates.blakadder.com/us.html)
- [esp-idf](https://github.com/espressif/esp-idf) is FreeRTOS based IOT firmware
- [node-red](https://nodered.org/) for low-code IOT device logic
- build esp8266 clone or compatible module and corner the hobbyist / 

### open source firmware
- [Tasmota](https://tasmota.github.io/docs/) open source firmware for esp8266/esp32 with OTA updates, MQTT, etc.
- [esphome](https://esphome.io/)
- [espurna](https://github.com/xoseperez/espurna)

### Tuya
- [tuya](https://www.tuya.com/) white box IOT manufacturer
    - OTA updates
    - device management
    - [low code product development](https://www.tuya.com/product/iot/zero-coding-development)
    - [turnkey IOT modules](https://solution.tuya.com/hardware)
    - 410,000 SKUs
    - they don't even have a wikipedia page
    - forget blink, sell 11ah based video module to tuya

### IDE
- [platformIO IDE](https://platformio.org/)

## joe / samsara
## rebase wireshark upstream
## put todos on monday
## fetch WFA tests and retest

## software

- openwrt support (config + updates)
- loci server (ui + ingress + db + tests)
- 11ah scalability
