# pyenv + pipenv

## build dependencies
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev

# build python 3.7.10
pyenv install 3.7.10

# make it the glopbal default
pyenv global 3.7.10

# install pipenv inside 3.7.10 pyenv
pip install pipenv

# install requirements
pipenv install -e .
