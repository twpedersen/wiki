# zephyr ipc service with rsc_table backend

problem statement:

    If you are willing to, I think that having a new IPC service backend using
    a resource table could be very welcome in upstream zephyr (instead of
    simply porting the sample).



on the remote, we read back the vring
```
struct fw_rsc_vdev_vring {
	uint32_t da;
	uint32_t align;
	uint32_t num;
	uint32_t notifyid;
	uint32_t reserved;
} METAL_PACKED_END;
	vring_rsc = rsc_table_get_vring0(rsc_table);
```
# Q: does master (linux) fill in the notifyid?
# A: yes, however it is just a unique (rproc-wide) index for the vring (ie. 0 and 1)



# Q: maybe ipc_service backend which relies on swirqs?
