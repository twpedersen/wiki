# environment setup

- [follow getting started page](https://docs.zephyrproject.org/latest/getting_started/index.html)


## cmake > 3.18 on debian stable (11)

add to `/etc/apt/sources.list`:
```
deb http://deb.debian.org/debian bullseye-backports main contrib non-free
```
then `apt-get update && apt-get install -t bullseye-backports cmake`

## fixup python3 pip3 user packages

I had to add the following to my `~/.zshrc`:
```
export PYTHONPATH="$HOME/.local/lib/python3.7/site-packages"
```

## what does USE_AMP=1 mean

- `/home/thomas/workspace2/zc706_2/ps7_cortexa9_1/standalone_domain/bsp/ps7_cortexa9_1/libsrc/scugic_v4_1/src/xscugic_hw.c`: don't initialize distributor
- `/home/thomas/workspace2/zc706_2/ps7_cortexa9_1/standalone_domain/bsp/ps7_cortexa9_1/libsrc/scugic_v4_1/src/xscugic.c`: don't initialize distributor
- `/home/thomas/workspace2/zc706_2/ps7_cortexa9_1/standalone_domain/bsp/ps7_cortexa9_1/libsrc/standalone_v7_1/src/boot.S`: don't initialize L2 cache
- `/home/thomas/workspace2/zc706_2/ps7_cortexa9_1/standalone_domain/bsp/ps7_cortexa9_1/libsrc/standalone_v7_1/src/translation_table.S`: mark DDR as inner-cachable only
- `/home/thomas/workspace2/zc706_2/ps7_cortexa9_1/standalone_domain/bsp/ps7_cortexa9_1/libsrc/standalone_v7_1/src/xil-crt0.S`: do not initialize (do not branch to XTime_SetTime) global timer
- `/home/thomas/workspace2/zc706_2/ps7_cortexa9_1/standalone_domain/bsp/ps7_cortexa9_1/libsrc/standalone_v7_1/src/xil_cache.c`: avoid all L2 cache operations

## zynq openamp remote requirements

- USE_AMP in boot.S, probably not even needed if we're not an ELF
- other BSP code needs to know which core is running
- need `zephyr,ipc` and `zephyr,ipc_shm` devicetree nodes

## zephyr aip hdp board

[zephyr sample application](https://github.com/zephyrproject-rtos/example-application)

## building libmetal for zynq7 from zephyr

libmetal lives in `modules/hal/libmetal`.

- **NOTE**: baremetal zynq7 cmake file: `libmetal/cmake/platforms/zynq7-generic.cmake`

currently the zephyr build:
    - sets `WITH_ZEPHYR=1` in `CMakeLists.txt`
    - `libmetal/cmake/syscheck.cmake` checks `WITH_ZEPHYR` and sets:
```
  if (CONFIG_CPU_CORTEX_M)
    set (MACHINE "cortexm" CACHE STRING "")
  endif (CONFIG_CPU_CORTEX_M)
```
    - which causes `libmetal/lib/system/zephyr/sys.h` to include `libmetal/lib/system/zephyr/cortexm/sys.h`

Add quick and dirty support for `zynq7`, by:
    - [x]: setting `MACHINE` to `zynq7` if `CONFIG_SOC_XILINX_XYNQ7000`
    - [x]: copy `zynq7` specific files from `libmetal/lib/system/generic/zynq7` (or `libmetal/lib/system/freertos/zynq7`, they seem the same) to `libmetal/lib/system/zephyr/zynq7`. We also need `../xlnx_common`

zephyr libmetal cmake variables:
```
CMAKE_SYSTEM_NAME=Generic
PROJECT_SYSTEM=zephyr
```

xilinx bsp location:

```
/tools/Xilinx/SDK/2018.3/data/embeddedsw/lib/bsp/standalone_v6_6/src/arm/cortexa9/xil_cache.h
/tools/Xilinx/SDK/2018.3/data/embeddedsw/XilinxProcessorIPLib/drivers/scugic_v3_8/src/xscugic.h
```

## build and run qemu example

```
west build -b qemu_cortex_a9 samples/hello_world
west build -t run
```

## menuconfig

```
west build -t menuconfig
```

## build openamp sample

- [openamp sample](https://docs.zephyrproject.org/latest/samples/subsys/ipc/openamp/README.html)
- [openamp sample resource table](https://docs.zephyrproject.org/latest/samples/subsys/ipc/openamp_rsc_table/README.html)

```
west build -b qemu_cortex_a9 samples/subsys/ipc/openamp_rsc_table
west build -t test
```

## TODO

- add zynq7000 2nd core board

