# goals

- OOB video over halow?
- video given some constraints?


# misc
- RTSP(UDP) video stream is bursty, need to determine the threshold above which we drop
- RTSP has out of band TCP stream ongoing
- RTSP client on aip ?
- iperf UDP ok up until about 4Mb/s
- packet loss? yes

# disable AQM

From u-boot:

```
sete aip_bootargs aip.disable_aqm=1
sete bootos linux
boot
```

# flash latest fw

aip_flash_fw data:/home/linux/os/latest/aipos_2021-11-29.tar.gz

# RTSP (samsara) iperf video
```
iperf -c sta02 -u --isochronous=1:1m,0.1 -t100
```

# detect missing sequence #s from pcap
```
tshark -r ~/sta02.cap -Y "ip.src eq 10.1.10.187" -Tfields -e frame.number -e wlan.seq -E separator=, > sta02.csv
```
open `sta02.csv` as spreadsheet and add the formula:

```
=IF(B3-B2=0, "", IF(B3-B2=1,"","missing"))
```


# reproduce

Fix MCS to 4 GI 1 on both sides.

on AP, start iperf:
```
iperf -s -i1 -u -e
```

on STA, start iperf in background:
```
iperf -c sta02 -u --isochronous=1:1m,0.1 -t100
 ```
observe close to 0% losses.

Then on STA, start a ping:
```
iperf -c sta02 -u --isochronous=1:1m,0.1 -t100
 ```

Observe we now lose about 1 packet every burst (second).

# try

- disable_sw_retry is 1??

# good settings

```
# driver: disable disabling sw_retry when mcs is fixed 
# driver: set # of TX buffers to 256
# driver: disable AQM

# fix MCS to 4 SGI
aip_set mcs 4
aip_set msg_gi 1

# disable A-MPDU
echo 1 > /sys/kernel/debug/ieee80211/phy0/wfa_test_mode
```

# TODO

- still disable_aqm? seeing a few drops
- rate control takes too long to converge
- test OTA?
- check up on nightly performance testing
- start testing on ribbon


# TX_STATUS backward compatibility

Retry count or TX count doesn't really matter, but what about TX_FAIL?

new hardware == clear TX_FAIL when a frame is sent
old hardware == set TX_FAIL when a frame wasn't acked

old kernel == treat TX_FAIL as not acked
new kernel == treat TX_FAIL as not transmitted and don't tell rate control

| kernel | hardware | works?                              |
|--------|----------|-------------------------------------|
| new    | new      | yes                                 |
| new    | old      | no, rate control is broken at least |

## upstream
- backports: patch feedback
- zephyr: custom IPI driver?

# kernel panic

```
[ 5012.282740] [<c0ae2b40>] (dev_hard_start_xmit) from [<c0ae3978>] (__dev_queue_xmit+0x99c/0xb74)
[ 5012.291448] [<c0ae3978>] (__dev_queue_xmit) from [<c0ae3b6c>] (dev_queue_xmit+0x1c/0x20)
[ 5012.299546] [<c0ae3b6c>] (dev_queue_xmit) from [<c0c23f34>] (br_dev_queue_push_xmit+0xdc/0x124)
[ 5012.308251] [<c0c23f34>] (br_dev_queue_push_xmit) from [<c0c23ffc>] (__br_forward+0x64/0xa4)
[ 5012.316695] [<c0c23ffc>] (__br_forward) from [<c0c244b4>] (br_flood+0x198/0x1a0)
[ 5012.324101] [<c0c244b4>] (br_flood) from [<c0c25e6c>] (br_handle_frame_finish+0x188/0x470)
[ 5012.332372] [<c0c25e6c>] (br_handle_frame_finish) from [<c0c262f4>] (br_handle_frame+0x1a0/0x34c)
[ 5012.341260] [<c0c262f4>] (br_handle_frame) from [<c0adb934>] (__netif_receive_skb_core+0x3d0/0xa40)
[ 5012.350319] [<c0adb934>] (__netif_receive_skb_core) from [<c0adc258>] (__netif_receive_skb_list_core+0x108/0x20c)
[ 5012.360595] [<c0adc258>] (__netif_receive_skb_list_core) from [<c0ae6bd0>] (netif_receive_skb_list+0x310/0x7f0)
[ 5012.370701] [<c0ae6bd0>] (netif_receive_skb_list) from [<c0d56258>] (ieee80211_rx_napi+0x1a8/0x1bc)
[ 5012.379766] [<c0d56258>] (ieee80211_rx_napi) from [<c0d0befc>] (ieee80211_tasklet_handler+0xd0/0xe0)
[ 5012.388913] [<c0d0befc>] (ieee80211_tasklet_handler) from [<c01307f8>] (tasklet_action_common.constprop.0+0x64/0xf0)
[ 5012.399446] [<c01307f8>] (tasklet_action_common.constprop.0) from [<c01308ac>] (tasklet_action+0x28/0x30)
[ 5012.409029] [<c01308ac>] (tasklet_action) from [<c010244c>] (__do_softirq+0x13c/0x5e8)
[ 5012.416959] [<c010244c>] (__do_softirq) from [<c0130180>] (irq_exit+0x148/0x180)
[ 5012.424368] [<c0130180>] (irq_exit) from [<c01921f0>] (__handle_domain_irq+0x80/0xd0)
[ 5012.432211] [<c01921f0>] (__handle_domain_irq) from [<c01022cc>] (gic_handle_irq+0x5c/0xa0)
[ 5012.440568] [<c01022cc>] (gic_handle_irq) from [<c0101a30>] (__irq_svc+0x70/0xb0)
[ 5012.448051] Exception stack(0xece03cf8 to 0xece03d40)
[ 5012.453105] 3ce0:                                                       00000000 ece2d970
[ 5012.461286] 3d00: 00001082 00000000 ece2d800 ece2d970 c1a07470 00000000 00000089 00000089
[ 5012.469471] 3d20: 00000000 ece03d5c 00000000 ece03d48 00001082 c0ac1410 600f0013 ffffffff
[ 5012.477665] [<c0101a30>] (__irq_svc) from [<c0ac1410>] (sock_wfree+0x38/0x84)
[ 5012.484819] [<c0ac1410>] (sock_wfree) from [<c0bb9628>] (unix_destruct_scm+0x90/0xe0)
[ 5012.492667] [<c0bb9628>] (unix_destruct_scm) from [<c0ac8e44>] (skb_release_head_state+0x84/0xb8)
[ 5012.501550] [<c0ac8e44>] (skb_release_head_state) from [<c0ac9380>] (consume_skb+0x84/0x1f0)
[ 5012.510003] [<c0ac9380>] (consume_skb) from [<c0acec48>] (skb_free_datagram+0x20/0x4c)
[ 5012.517936] [<c0acec48>] (skb_free_datagram) from [<c0bbb538>] (unix_dgram_recvmsg+0x2f4/0x454)
[ 5012.526651] [<c0bbb538>] (unix_dgram_recvmsg) from [<c0ab93dc>] (sock_read_iter+0xac/0xfc)
[ 5012.534930] [<c0ab93dc>] (sock_read_iter) from [<c029f120>] (__vfs_read+0x124/0x170)
[ 5012.542682] [<c029f120>] (__vfs_read) from [<c029f210>] (vfs_read+0xa4/0x118)
[ 5012.549824] [<c029f210>] (vfs_read) from [<c029f740>] (ksys_read+0x5c/0xc8)
[ 5012.556794] [<c029f740>] (ksys_read) from [<c029f7c4>] (sys_read+0x18/0x1c)
[ 5012.563765] [<c029f7c4>] (sys_read) from [<c0101000>] (ret_fast_syscall+0x0/0x28)
```
