# partial AID/BSSID

- S1G Sounding NDP
- slot assignment
- NDP_1M/NDP_2M CTS
- NDP_1M CF_END
- NDP PS-Poll
- NDP Paging


# phobos TWT


**u.xTCP.txStream** is NULL?

Add first packet (1167 bytes) to socket->u.xTCP.txStream **IPTask should process**

FreeRTOS_Socket.c:
```c
xByteCount = ( BaseType_t ) uxStreamBufferAdd( pxSocket->u.xTCP.txStream, 0UL, pucSource, ( size_t ) xByteCount );
...
/* notify IPTask */
( void ) xSendEventToIPTask( eTCPTimerEvent );
...
/* wait until packet has been sent */
            ( void ) xEventGroupWaitBits( pxSocket->xEventGroup, ( EventBits_t ) eSOCKET_SEND | ( EventBits_t ) eSOCKET_CLOSED,
                                                          pdTRUE /*xClearOnExit*/, pdFALSE /*xWaitAllBits*/, xRemainingTime );
```

IPTask gets the `eTCPTImerEvent`, which simply marks TCPTimer as expired:
```c
    /* Simply mark the TCP timer as expired so it gets processed
     * the next time prvCheckNetworkTimers() is called. */
     xTCPTimer.bExpired = pdTRUE_UNSIGNE
```

`FreeRTOS_IP.c` checks the timers:
```c
            if( xCheckTCPSockets != pdFALSE )
            {
                /* Attend to the sockets, returning the period after which the
                 * check must be repeated. */
                xNextTime = xTCPTimerCheck( xWillSleep );
                prvIPTimerStart( &xTCPTimer, xNextTime );
                xProcessedTCPMessage = 0;
            }
```

`FreeRTOS_TCP_IP.c`
```c
    BaseType_t xTCPSocketCheck( FreeRTOS_Socket_t * pxSocket )
...
    static int32_t prvTCPSendRepeated( FreeRTOS_Socket_t * pxSocket,
                                       NetworkBufferDescriptor_t ** ppxNetworkBuffer )
    {
...
           xSendLength = prvTCPPrepareSend( pxSocket, ppxNetworkBuffer, uxOptionsLength ); // returns 0!
```

// windowsize is 1000, but we want to send 1160...

# debug twt


do sw reset after q stop:
aip_reg 50 0x1

# phobos test issues

1. failed to boot (no prompt)
    - see this on posix and hdp
    - can trigger if '\r' is sent too quickly after boot (?)

2. occupied skb in slot (hdp)
    - per?

3. uart command corruption (`iperf -c 10.11.1.11 ...` -> `iperf -c 10.11`)
    - issue when writing?
    - at least hdp, issue on posix?


# hostap entropy

`aip_reg 113` gives a random number.

We can feed entropy to the kernel, but for security it won't increment the
available entropy count when just writing to `/dev/random`.

Get available entropy count in bits:

    cat /proc/sys/kernel/random/entropy_avail

[This small utility can add entropy count](https://github.com/rfinnie/twuewand/tree/main/rndaddentropy)

# recommended default `aip_board_init`

aip_reg 2 0x770b000b
aip_reg 3 0x12080000
aip_set tx_att 10
aip_set enable_fir 0
aip_reg 119 0x40040


# hostap TODOs

- stop using FreeRTOS specific events?

# limit cores available to a process

phobos posix tests consistently lock up on the build server (4 threads)

To reproduce on our work station, we need to limit the number of cores available to phobos.

    sudo apt-get install cgroup-tools
    sudo cgcreate -g cpuset:/lesscores
    sudo cgset -r cpuset.cpus="0-3" lesscores
    sudo cgset -r cpuset.mems=0 lesscores
    sudo cgexec -g cpuset:lesscores ./build/phobos -a -i phobos1
    sudo cgexec -g cpuset:lesscores ./build/phobos -t

Well..

# posix port "lockup"

- each FreeRTOS task is implemented as a pthread, but not run concurrently.
- pthread is created then immediately suspended
- when task blocks it calls vPortYield()

"lockup" is when all threads are waiting to be woken up in prvSuspendSelf() -> event_wait()

The actual pthreads:
- frl_rx_thread is blocked on recv()
- frl_tx_thread blocked on event_wait_timed() -> clock_gettime()

Sample WIFI_Off() hang:

(hangs on gdb `step`)
```
#0  futex_wait_cancelable (private=0, expected=0, futex_word=0x7f777000daf0) at ../sysdeps/unix/sysv/linux/futex-internal.h:88
#1  __pthread_cond_wait_common (abstime=0x0, mutex=0x7f777000daa0, cond=0x7f777000dac8) at pthread_cond_wait.c:502
#2  __pthread_cond_wait (cond=0x7f777000dac8, mutex=0x7f777000daa0) at pthread_cond_wait.c:655
#3  0x000055731a2fdc48 in event_wait (ev=0x7f777000daa0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:37
#4  0x000055731a2fe3d4 in prvSuspendSelf (thread=0x7f7774455fe8)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:491
#5  0x000055731a2fe3aa in prvSwitchThread (pxThreadToResume=0x7f777000d758, pxThreadToSuspend=0x7f7774455fe8)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:469
#6  0x000055731a2fe0d7 in vPortYieldFromISR () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:279
#7  0x000055731a2fe0e8 in vPortYield () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:287
#8  0x000055731a2f287f in xQueueGenericSend (xQueue=0x7f7770003210, pvItemToQueue=0x55731a3c2260 <xNetworkDownEvent.4720>, xTicksToWait=0, xCopyPosition=0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/queue.c:843
#9  0x000055731a3037a0 in xSendEventStructToIPTask (pxEvent=0x55731a3c2260 <xNetworkDownEvent.4720>, uxTimeout=0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS-Plus/Source/FreeRTOS-Plus-TCP/FreeRTOS_IP.c:1490
#10 0x000055731a302f0f in FreeRTOS_NetworkDown () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS-Plus/Source/FreeRTOS-Plus-TCP/FreeRTOS_IP.c:907
#11 0x000055731a3278f6 in WIFI_Off () at ../freertos/iot_wifi.c:93
```

scheduler:

(eventually hangs on gdb `step` somehwere)
```
#0  0x00007f7776c974cc in __GI___sigtimedwait (set=set@entry=0x7ffe57acdba0, info=info@entry=0x7ffe57acdaf0, timeout=timeout@entry=0x0)
    at ../sysdeps/unix/sysv/linux/sigtimedwait.c:29
#1  0x00007f7776e322bc in __sigwait (set=0x7ffe57acdba0, sig=0x7ffe57acdc2c) at ../sysdeps/unix/sysv/linux/sigwait.c:28
#2  0x000055731a2fdf40 in xPortStartScheduler () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:197
#3  0x000055731a2f9828 in vTaskStartScheduler () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/tasks.c:2087
#4  0x000055731a2f0bf3 in app_phobos () at main.c:286
```

timer:
```
#0  futex_wait_cancelable (private=0, expected=0, futex_word=0x55731afaa440) at ../sysdeps/unix/sysv/linux/futex-internal.h:88
#1  __pthread_cond_wait_common (abstime=0x0, mutex=0x55731afaa3f0, cond=0x55731afaa418) at pthread_cond_wait.c:502
#2  __pthread_cond_wait (cond=0x55731afaa418, mutex=0x55731afaa3f0) at pthread_cond_wait.c:655
#3  0x000055731a2fdc48 in event_wait (ev=0x55731afaa3f0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:37
#4  0x000055731a2fe3d4 in prvSuspendSelf (thread=0x55731a429c58 <uxTimerTaskStack+16344>)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:491
#5  0x000055731a2fe3aa in prvSwitchThread (pxThreadToResume=0x55731afaf248, pxThreadToSuspend=0x55731a429c58 <uxTimerTaskStack+16344>)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:469
#6  0x000055731a2fe0d7 in vPortYieldFromISR () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:279
#7  0x000055731a2fe0e8 in vPortYield () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:287
#8  0x000055731a2fd1b2 in prvProcessTimerOrBlockTask (xNextExpireTime=0, xListWasEmpty=1)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/timers.c:633
#9  0x000055731a2fd0f8 in prvTimerTask (pvParameters=0x0) at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/timers.c:579
```

IPTask got a signal while in event_wait
```
#0  futex_wait_cancelable (private=0, expected=0, futex_word=0x55731afaa2b0) at ../sysdeps/unix/sysv/linux/futex-internal.h:88
#1  __pthread_cond_wait_common (abstime=0x0, mutex=0x55731afaa260, cond=0x55731afaa288) at pthread_cond_wait.c:502
#2  __pthread_cond_wait (cond=0x55731afaa288, mutex=0x55731afaa260) at pthread_cond_wait.c:655
#3  0x000055731a2fdc48 in event_wait (ev=0x55731afaa260)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:37
#4  0x000055731a2fe3d4 in prvSuspendSelf (thread=0x55731a41c9b8 <uxIdleTaskStack.5068+8152>)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:491
#5  0x000055731a2fe3aa in prvSwitchThread (pxThreadToResume=0x7f7774455fe8, pxThreadToSuspend=0x55731a41c9b8 <uxIdleTaskStack.5068+8152>)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:469
#6  0x000055731a2fe263 in vPortSystemTickHandler (sig=14)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:397
#7  <signal handler called>
#8  futex_wait_cancelable (private=0, expected=0, futex_word=0x7f777000d8a0) at ../sysdeps/unix/sysv/linux/futex-internal.h:88
#9  __pthread_cond_wait_common (abstime=0x0, mutex=0x7f777000d850, cond=0x7f777000d878) at pthread_cond_wait.c:502
#10 __pthread_cond_wait (cond=0x7f777000d878, mutex=0x7f777000d850) at pthread_cond_wait.c:655
#11 0x000055731a2fdc48 in event_wait (ev=0x7f777000d850)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:37
#12 0x000055731a2fe3d4 in prvSuspendSelf (thread=0x7f777000d758)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:491
#13 0x000055731a2fe3aa in prvSwitchThread (pxThreadToResume=0x55731a41c9b8 <uxIdleTaskStack.5068+8152>, pxThreadToSuspend=0x7f777000d758)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:469
#14 0x000055731a2fe0d7 in vPortYieldFromISR () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:279
#15 0x000055731a2fe0e8 in vPortYield () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:287
#16 0x000055731a2f36a1 in xQueueReceive (xQueue=0x7f7770003210, pvBuffer=0x7f777000c5d0, xTicksToWait=699)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/queue.c:1425
#17 0x000055731a302a9e in prvIPTask (pvParameters=0x0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS-Plus/Source/FreeRTOS-Plus-TCP/FreeRTOS_IP.c:414
#18 0x000055731a2fe33e in prvWaitForStart (pvParams=0x7f777000d758)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:435
```

# freertos AP wpa_freertos_lock() assert

**NB** pthread_cond_wait() is called while pthread_mutex_lock() is held.

In this case, 2 different threads were handling the system tick signal (!)

---

**tx_thread signal**:
```
#0  futex_wait_cancelable (private=0, expected=0, futex_word=0x55d988f5d2b0) at ../sysdeps/unix/sysv/linux/futex-internal.h:88
#1  __pthread_cond_wait_common (abstime=0x0, mutex=0x55d988f5d260, cond=0x55d988f5d288) at pthread_cond_wait.c:502
#2  __pthread_cond_wait (cond=0x55d988f5d288, mutex=0x55d988f5d260) at pthread_cond_wait.c:655
#3  0x000055d988307c48 in event_wait (ev=0x55d988f5d260)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:37
#4  0x000055d988308420 in prvSuspendSelf (thread=0x55d9884269b8 <uxIdleTaskStack.5068+8152>)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:490
#5  0x000055d9883083f6 in prvSwitchThread (pxThreadToResume=0x7f5cb8016658, pxThreadToSuspend=0x55d9884269b8 <uxIdleTaskStack.5068+8152>)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:468
#6  0x000055d9883082af in vPortSystemTickHandler (sig=14)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:396
#7  <signal handler called>
#8  0x000055d9882fa340 in pthread_mutex_unlock@plt ()
#9  0x000055d988307d1e in event_wait_timed (ev=0x7f5cb80123d0, ms=1000)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:66
#10 0x000055d9883c6cd5 in frl_tx_thread (pvParam=0x7f5cb8002290) at ../src/drivers/driver_fr_linux.c:160
#11 0x00007f5cc94c1fa3 in start_thread (arg=<optimized out>) at pthread_create.c:486
#12 0x00007f5cc93f24cf in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:95
```

**idle signal:**
```
#0  futex_wait_cancelable (private=0, expected=0, futex_word=0x7f5cc0002e40) at ../sysdeps/unix/sysv/linux/futex-internal.h:88
#1  __pthread_cond_wait_common (abstime=0x0, mutex=0x7f5cc0002df0, cond=0x7f5cc0002e18) at pthread_cond_wait.c:502
#2  __pthread_cond_wait (cond=0x7f5cc0002e18, mutex=0x7f5cc0002df0) at pthread_cond_wait.c:655
#3  0x000055d988307c48 in event_wait (ev=0x7f5cc0002df0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:37
#4  0x000055d988308420 in prvSuspendSelf (thread=0x7f5cc0002cf8)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:490
#5  0x000055d9883083f6 in prvSwitchThread (pxThreadToResume=0x7f5cc000d758, pxThreadToSuspend=0x7f5cc0002cf8)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:468
#6  0x000055d9883082af in vPortSystemTickHandler (sig=14)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:396
#7  <signal handler called>
#8  0x00007f5cc94cbbf0 in __GI___nanosleep (requested_time=0x7f5cc8af3e90, remaining=0x0) at ../sysdeps/unix/sysv/linux/nanosleep.c:28
#9  0x000055d9882fad42 in vApplicationIdleHook () at main.c:385
#10 0x000055d988304cca in prvIdleTask (pvParameters=0x0) at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/tasks.c:3483
#11 0x000055d98830838a in prvWaitForStart (pvParams=0x55d9884269b8 <uxIdleTaskStack.5068+8152>)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:434
#12 0x00007f5cc94c1fa3 in start_thread (arg=<optimized out>) at pthread_create.c:486
#13 0x00007f5cc93f24cf in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:95
```
---

**prvIPTask hangs:**
```
#0  0x000055d9883329c8 in wpa_assert_loop (func_name=0x55d9883d0e80 <__FUNCTION__.13421> "wpa_freertos_lock", err_line=220) at ../freertos/wpa_freertos.c:288
288     ../freertos/wpa_freertos.c: No such file or directory.
(gdb) bt
#0  0x000055d9883329c8 in wpa_assert_loop (func_name=0x55d9883d0e80 <__FUNCTION__.13421> "wpa_freertos_lock", err_line=220) at ../freertos/wpa_freertos.c:288
#1  0x000055d9883327f0 in wpa_freertos_lock () at ../freertos/wpa_freertos.c:220
#2  0x000055d988332e26 in xNetworkInterfaceOutput (pxNetworkBuffer=0x55d9884275b0 <xNetworkBufferDescriptors+144>, xReleaseAfterSend=1)
    at ../freertos/freertos_tcp.c:38
#3  0x000055d988309feb in FreeRTOS_OutputARPRequest (ulIPAddress=185273098)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS-Plus/Source/FreeRTOS-Plus-TCP/FreeRTOS_ARP.c:754
#4  0x000055d988309f6b in vARPAgeCache () at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS-Plus/Source/FreeRTOS-Plus-TCP/FreeRTOS_ARP.c:693
#5  0x000055d98830cb80 in prvIPTask (pvParameters=0x0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS-Plus/Source/FreeRTOS-Plus-TCP/FreeRTOS_IP.c:468
#6  0x000055d98830838a in prvWaitForStart (pvParams=0x7f5cc000d758)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:434
#7  0x00007f5cc94c1fa3 in start_thread (arg=<optimized out>) at pthread_create.c:486
#8  0x00007f5cc93f24cf in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:95
```

**WIFITask blocked on pthread_mutex_lock when waiting for TX from eloop**
```
#0  __lll_lock_wait () at ../sysdeps/unix/sysv/linux/x86_64/lowlevellock.S:103
#1  0x00007f5cc94c4714 in __GI___pthread_mutex_lock (mutex=0x7f5cb80123d0) at ../nptl/pthread_mutex_lock.c:80
#2  0x000055d988307d3d in event_signal (ev=0x7f5cb80123d0)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/utils/wait_for_event.c:72
#3  0x000055d9883c78c1 in wpa_driver_frl_tx (priv=0x7f5cb8002290, nb=0x55d988460c60 <netbuf_pool+768>) at ../src/drivers/driver_fr_linux.c:542
#4  0x000055d9883774f4 in wpa_mac_send_mlme (wpa_s=0x7f5cb8000e00, data=0x7f5cb8019aa0 "\260", data_len=30, noack=0, freq=0, csa_offs=0x0, csa_offs_len=0,
    no_encrypt=0, wait=0) at ../src/mac/tx.c:112
#5  0x000055d9883c69f4 in wpa_thinmac_send_mlme (priv=0x7f5cb8002290, data=0x7f5cb8019aa0 "\260", data_len=30, noack=0, freq=0, csa_offs=0x0, csa_offs_len=0,
    no_encrypt=0, wait=0) at ../src/drivers/driver_thinmac.h:56
#6  0x000055d9883967ee in hostapd_drv_send_mlme (hapd=0x7f5cb80183c0, msg=0x7f5cb8019aa0, len=30, noack=0, csa_offs=0x0, csa_offs_len=0, no_encrypt=0)
    at ../src/ap/ap_drv_ops.c:756
#7  0x000055d9883a3b28 in send_auth_reply (hapd=0x7f5cb80183c0, sta=0x7f5cb801a3c0, dst=0x7f5cbc00133a "\332&\272X\242[\262\211tLe\221",
    bssid=0x7f5cbc001340 "\262\211tLe\221", auth_alg=0, auth_transaction=2, resp=0, ies=0x7f5cc7af1900 '\377' <repeats 48 times>, ies_len=0,
    dbg=0x55d9883e6d10 "handle-auth") at ../src/ap/ieee802_11.c:513
#8  0x000055d9883a4a54 in handle_auth (hapd=0x7f5cb80183c0, mgmt=0x7f5cbc001330, len=30, rssi=0, from_queue=0) at ../src/ap/ieee802_11.c:2720
#9  0x000055d9883a8200 in ieee802_11_mgmt (hapd=0x7f5cb80183c0, buf=0x7f5cbc001330 "\260", len=30, fi=0x7f5cc7af1a70) at ../src/ap/ieee802_11.c:5087
#10 0x000055d9883795c5 in ap_mgmt_rx (ctx=0x7f5cb8000e00, rx_mgmt=0x7f5cc7af1ba0) at ap.c:1030
#11 0x000055d988361da2 in wpa_supplicant_event (ctx=0x7f5cb8000e00, event=EVENT_RX_MGMT, data=0x7f5cc7af1ba0) at events.c:4982
#12 0x000055d9883c8a07 in wpa_rx_mgmt (wpa_s=0x7f5cb8000e00, nb=0x55d988460b60 <netbuf_pool+512>) at ../src/mac/rx.c:77
#13 0x000055d9883c8aa4 in wpa_rx (wpa_s=0x7f5cb8000e00, nb=0x55d988460b60 <netbuf_pool+512>) at ../src/mac/rx.c:96
#14 0x000055d9883c6df9 in frl_rx_process (eloop_ctx=0x7f5cb8002290, user_ctx=0x55d988460b60 <netbuf_pool+512>) at ../src/drivers/driver_fr_linux.c:185
#15 0x000055d988369f32 in eloop_run () at ../src/utils/eloop_freertos.c:357
#16 0x000055d988340321 in wpa_supplicant_run (global=0x7f5cb8000b20) at wpa_supplicant.c:7056
#17 0x000055d9883324a3 in WIFITask (arg=0x55d988424420 <wifi_opts>) at ../freertos/wpa_freertos.c:92
#18 0x000055d98830838a in prvWaitForStart (pvParams=0x7f5cc0002cf8)
    at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c:434
#19 0x00007f5cc94c1fa3 in start_thread (arg=<optimized out>) at pthread_create.c:486
#20 0x00007f5cc93f24cf in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:95
```
# freertos POSIX port

The interface a port must implement is defined in `FreeRTOS/FreeRTOS/Source/include/portable.h`

The POSIX port implementation is in `FreeRTOS/FreeRTOS/Source/portable/ThirdParty/GCC/Posix/port.c`

## task creation

xTaskCreate()
    -> prvInitialiseNewTask()
        -> pxPortInitialiseStack()
            -> pthread_once(.., pvrSetupSignalsAndSchedulerPolicy)
                -> prvSetupSignalsAndSchedulerPolicy()
                    -> block all signals execpt SIGINT (for GDB)
                    -> SIG_RESUME (USR1) is to be ignored
                    -> SIGALRM triggers system tick

## task ready

tasks go to ready by `prvAddTaskToReadyList( pxTCB )`

## task blocked
`prvAddCurrentTaskToDelayedList(pxTCB)`

## why is preemption on worse?

because the system tick handler can do a context switch (put itself asleep on
event_wait()) , and it seemingly gets triggered when signals should've been
blocked (event_wait() with uxCriticalNesting == 2).

## scheduling

- pxReadyTasksLists
- when a task blocks it calls vPortYield()
- this swaps the next task in the ready list

# try
- disable configUSE_TASK_NOTIFICATIONS

# dump all make variables

    $(foreach v, $(.VARIABLES), $(info $(v) = $($(v)))) 

# S1G assoc

request:
- AID request
- S1G capabilities

response:
- AID response
- S1G Capabilities
- S1G Operation


struct wpa_rx_desc {
   int rates;
   struct netbuf *nb;
}

# wpa_s breakpoint

## cannot break

Signal        Stop	Print	Pass to program	Description

SIGHUP        Yes	Yes	Yes		Hangup
SIGINT        Yes	Yes	No		Interrupt
SIGQUIT       Yes	Yes	Yes		Quit
SIGILL        Yes	Yes	Yes		Illegal instruction
SIGTRAP       Yes	Yes	No		Trace/breakpoint trap
SIGABRT       Yes	Yes	Yes		Aborted
SIGEMT        Yes	Yes	Yes		Emulation trap
SIGFPE        Yes	Yes	Yes		Arithmetic exception
SIGKILL       Yes	Yes	Yes		Killed
SIGBUS        Yes	Yes	Yes		Bus error
SIGSEGV       Yes	Yes	Yes		Segmentation fault
SIGSYS        Yes	Yes	Yes		Bad system call
SIGPIPE       Yes	Yes	Yes		Broken pipe
SIGALRM       No	No	Yes		Alarm clock
SIGTERM       Yes	Yes	Yes		Terminated
SIGURG        No	No	Yes		Urgent I/O condition
SIGSTOP       Yes	Yes	Yes		Stopped (signal)
SIGTSTP       Yes	Yes	Yes		Stopped (user)
SIGCONT       Yes	Yes	Yes		Continued
SIGCHLD       No	No	Yes		Child status changed
SIGTTIN       Yes	Yes	Yes		Stopped (tty input)
SIGTTOU       Yes	Yes	Yes		Stopped (tty output)
SIGIO         No	No	Yes		I/O possible
SIGXCPU       Yes	Yes	Yes		CPU time limit exceeded
SIGXFSZ       Yes	Yes	Yes		File size limit exceeded
SIGVTALRM     No	No	Yes		Virtual timer expired
SIGPROF       No	No	Yes		Profiling timer expired
SIGWINCH      No	No	Yes		Window size changed
SIGLOST       Yes	Yes	Yes		Resource lost
SIGUSR1       Yes	Yes	Yes		User defined signal 1
SIGUSR2       Yes	Yes	Yes		User defined signal 2
SIGPWR        Yes	Yes	Yes		Power fail/restart
SIGPOLL       No	No	Yes		Pollable event occurred
SIGWIND       Yes	Yes	Yes		SIGWIND
SIGPHONE      Yes	Yes	Yes		SIGPHONE
SIGWAITING    No	No	Yes		Process's LWPs are blocked
SIGLWP        No	No	Yes		Signal LWP
SIGDANGER     Yes	Yes	Yes		Swap space dangerously low
SIGGRANT      Yes	Yes	Yes		Monitor mode granted
SIGRETRACT    Yes	Yes	Yes		Need to relinquish monitor mode
SIGMSG        Yes	Yes	Yes		Monitor mode data available
SIGSOUND      Yes	Yes	Yes		Sound completed
SIGSAK        Yes	Yes	Yes		Secure attention
SIGPRIO       No	No	Yes		SIGPRIO
SIG33         Yes	Yes	Yes		Real-time event 33
SIG34         Yes	Yes	Yes		Real-time event 34
SIG35         Yes	Yes	Yes		Real-time event 35
SIG36         Yes	Yes	Yes		Real-time event 36
SIG37         Yes	Yes	Yes		Real-time event 37
SIG38         Yes	Yes	Yes		Real-time event 38
SIG39         Yes	Yes	Yes		Real-time event 39
SIG40         Yes	Yes	Yes		Real-time event 40
SIG41         Yes	Yes	Yes		Real-time event 41
SIG42         Yes	Yes	Yes		Real-time event 42
SIG43         Yes	Yes	Yes		Real-time event 43
SIG44         Yes	Yes	Yes		Real-time event 44
SIG45         Yes	Yes	Yes		Real-time event 45
SIG46         Yes	Yes	Yes		Real-time event 46
SIG47         Yes	Yes	Yes		Real-time event 47
SIG48         Yes	Yes	Yes		Real-time event 48
SIG49         Yes	Yes	Yes		Real-time event 49
SIG50         Yes	Yes	Yes		Real-time event 50
SIG51         Yes	Yes	Yes		Real-time event 51
SIG52         Yes	Yes	Yes		Real-time event 52
SIG53         Yes	Yes	Yes		Real-time event 53
SIG54         Yes	Yes	Yes		Real-time event 54
SIG55         Yes	Yes	Yes		Real-time event 55
SIG56         Yes	Yes	Yes		Real-time event 56
SIG57         Yes	Yes	Yes		Real-time event 57
SIG58         Yes	Yes	Yes		Real-time event 58
SIG59         Yes	Yes	Yes		Real-time event 59
SIG60         Yes	Yes	Yes		Real-time event 60
SIG61         Yes	Yes	Yes		Real-time event 61
SIG62         Yes	Yes	Yes		Real-time event 62
SIG63         Yes	Yes	Yes		Real-time event 63
SIGCANCEL     No	No	Yes		LWP internal signal
SIG32         Yes	Yes	Yes		Real-time event 32
SIG64         Yes	Yes	Yes		Real-time event 64
SIG65         Yes	Yes	Yes		Real-time event 65
SIG66         Yes	Yes	Yes		Real-time event 66
SIG67         Yes	Yes	Yes		Real-time event 67
SIG68         Yes	Yes	Yes		Real-time event 68
SIG69         Yes	Yes	Yes		Real-time event 69
SIG70         Yes	Yes	Yes		Real-time event 70
SIG71         Yes	Yes	Yes		Real-time event 71
SIG72         Yes	Yes	Yes		Real-time event 72
SIG73         Yes	Yes	Yes		Real-time event 73
SIG74         Yes	Yes	Yes		Real-time event 74
SIG75         Yes	Yes	Yes		Real-time event 75
SIG76         Yes	Yes	Yes		Real-time event 76
SIG77         Yes	Yes	Yes		Real-time event 77
SIG78         Yes	Yes	Yes		Real-time event 78
SIG79         Yes	Yes	Yes		Real-time event 79
SIG80         Yes	Yes	Yes		Real-time event 80
SIG81         Yes	Yes	Yes		Real-time event 81
SIG82         Yes	Yes	Yes		Real-time event 82
SIG83         Yes	Yes	Yes		Real-time event 83
SIG84         Yes	Yes	Yes		Real-time event 84
SIG85         Yes	Yes	Yes		Real-time event 85
SIG86         Yes	Yes	Yes		Real-time event 86
SIG87         Yes	Yes	Yes		Real-time event 87
SIG88         Yes	Yes	Yes		Real-time event 88
SIG89         Yes	Yes	Yes		Real-time event 89
SIG90         Yes	Yes	Yes		Real-time event 90
SIG91         Yes	Yes	Yes		Real-time event 91
SIG92         Yes	Yes	Yes		Real-time event 92
SIG93         Yes	Yes	Yes		Real-time event 93
SIG94         Yes	Yes	Yes		Real-time event 94
SIG95         Yes	Yes	Yes		Real-time event 95
SIG96         Yes	Yes	Yes		Real-time event 96
SIG97         Yes	Yes	Yes		Real-time event 97
SIG98         Yes	Yes	Yes		Real-time event 98
SIG99         Yes	Yes	Yes		Real-time event 99
SIG100        Yes	Yes	Yes		Real-time event 100
SIG101        Yes	Yes	Yes		Real-time event 101
SIG102        Yes	Yes	Yes		Real-time event 102
SIG103        Yes	Yes	Yes		Real-time event 103
SIG104        Yes	Yes	Yes		Real-time event 104
SIG105        Yes	Yes	Yes		Real-time event 105
SIG106        Yes	Yes	Yes		Real-time event 106
SIG107        Yes	Yes	Yes		Real-time event 107
SIG108        Yes	Yes	Yes		Real-time event 108
SIG109        Yes	Yes	Yes		Real-time event 109
SIG110        Yes	Yes	Yes		Real-time event 110
SIG111        Yes	Yes	Yes		Real-time event 111
SIG112        Yes	Yes	Yes		Real-time event 112
SIG113        Yes	Yes	Yes		Real-time event 113
SIG114        Yes	Yes	Yes		Real-time event 114
SIG115        Yes	Yes	Yes		Real-time event 115
SIG116        Yes	Yes	Yes		Real-time event 116
SIG117        Yes	Yes	Yes		Real-time event 117
SIG118        Yes	Yes	Yes		Real-time event 118
SIG119        Yes	Yes	Yes		Real-time event 119
SIG120        Yes	Yes	Yes		Real-time event 120
SIG121        Yes	Yes	Yes		Real-time event 121
SIG122        Yes	Yes	Yes		Real-time event 122
SIG123        Yes	Yes	Yes		Real-time event 123
SIG124        Yes	Yes	Yes		Real-time event 124
SIG125        Yes	Yes	Yes		Real-time event 125
SIG126        Yes	Yes	Yes		Real-time event 126
SIG127        Yes	Yes	Yes		Real-time event 127
SIGINFO       Yes	Yes	Yes		Information request
EXC_BAD_ACCESS Yes	Yes	Yes		Could not access memory
EXC_BAD_INSTRUCTION Yes	Yes	Yes		Illegal instruction/operand
EXC_ARITHMETIC Yes	Yes	Yes		Arithmetic exception
EXC_EMULATION Yes	Yes	Yes		Emulation instruction
EXC_SOFTWARE  Yes	Yes	Yes		Software generated exception
EXC_BREAKPOINT Yes	Yes	Yes		Breakpoint
SIGLIBRT      No	No	Yes		librt internal signal

sigset: 0x0

## can break

SIGHUP        Yes	Yes	Yes		Hangup
SIGINT        Yes	Yes	No		Interrupt
SIGQUIT       Yes	Yes	Yes		Quit
SIGILL        Yes	Yes	Yes		Illegal instruction
SIGTRAP       Yes	Yes	No		Trace/breakpoint trap
SIGABRT       Yes	Yes	Yes		Aborted
SIGEMT        Yes	Yes	Yes		Emulation trap
SIGFPE        Yes	Yes	Yes		Arithmetic exception
SIGKILL       Yes	Yes	Yes		Killed
SIGBUS        Yes	Yes	Yes		Bus error
SIGSEGV       Yes	Yes	Yes		Segmentation fault
SIGSYS        Yes	Yes	Yes		Bad system call
SIGPIPE       Yes	Yes	Yes		Broken pipe
SIGALRM       No	No	Yes		Alarm clock
SIGTERM       Yes	Yes	Yes		Terminated
SIGURG        No	No	Yes		Urgent I/O condition
SIGSTOP       Yes	Yes	Yes		Stopped (signal)
SIGTSTP       Yes	Yes	Yes		Stopped (user)
SIGCONT       Yes	Yes	Yes		Continued
SIGCHLD       No	No	Yes		Child status changed
SIGTTIN       Yes	Yes	Yes		Stopped (tty input)
SIGTTOU       Yes	Yes	Yes		Stopped (tty output)
SIGIO         No	No	Yes		I/O possible
SIGXCPU       Yes	Yes	Yes		CPU time limit exceeded
SIGXFSZ       Yes	Yes	Yes		File size limit exceeded
SIGVTALRM     No	No	Yes		Virtual timer expired
SIGPROF       No	No	Yes		Profiling timer expired
SIGWINCH      No	No	Yes		Window size changed
SIGLOST       Yes	Yes	Yes		Resource lost
SIGUSR1       Yes	Yes	Yes		User defined signal 1
SIGUSR2       Yes	Yes	Yes		User defined signal 2
SIGPWR        Yes	Yes	Yes		Power fail/restart
SIGPOLL       No	No	Yes		Pollable event occurred
SIGWIND       Yes	Yes	Yes		SIGWIND
SIGPHONE      Yes	Yes	Yes		SIGPHONE
SIGWAITING    No	No	Yes		Process's LWPs are blocked
SIGLWP        No	No	Yes		Signal LWP
SIGDANGER     Yes	Yes	Yes		Swap space dangerously low
SIGGRANT      Yes	Yes	Yes		Monitor mode granted
SIGRETRACT    Yes	Yes	Yes		Need to relinquish monitor mode
SIGMSG        Yes	Yes	Yes		Monitor mode data available
SIGSOUND      Yes	Yes	Yes		Sound completed
SIGSAK        Yes	Yes	Yes		Secure attention
SIGPRIO       No	No	Yes		SIGPRIO
SIG33         Yes	Yes	Yes		Real-time event 33
SIG34         Yes	Yes	Yes		Real-time event 34
SIG35         Yes	Yes	Yes		Real-time event 35
SIG36         Yes	Yes	Yes		Real-time event 36
SIG37         Yes	Yes	Yes		Real-time event 37
SIG38         Yes	Yes	Yes		Real-time event 38
SIG39         Yes	Yes	Yes		Real-time event 39
SIG40         Yes	Yes	Yes		Real-time event 40
SIG41         Yes	Yes	Yes		Real-time event 41
SIG42         Yes	Yes	Yes		Real-time event 42
SIG43         Yes	Yes	Yes		Real-time event 43
SIG44         Yes	Yes	Yes		Real-time event 44
SIG45         Yes	Yes	Yes		Real-time event 45
SIG46         Yes	Yes	Yes		Real-time event 46
SIG47         Yes	Yes	Yes		Real-time event 47
SIG48         Yes	Yes	Yes		Real-time event 48
SIG49         Yes	Yes	Yes		Real-time event 49
SIG50         Yes	Yes	Yes		Real-time event 50
SIG51         Yes	Yes	Yes		Real-time event 51
SIG52         Yes	Yes	Yes		Real-time event 52
SIG53         Yes	Yes	Yes		Real-time event 53
SIG54         Yes	Yes	Yes		Real-time event 54
SIG55         Yes	Yes	Yes		Real-time event 55
SIG56         Yes	Yes	Yes		Real-time event 56
SIG57         Yes	Yes	Yes		Real-time event 57
SIG58         Yes	Yes	Yes		Real-time event 58
SIG59         Yes	Yes	Yes		Real-time event 59
SIG60         Yes	Yes	Yes		Real-time event 60
SIG61         Yes	Yes	Yes		Real-time event 61
SIG62         Yes	Yes	Yes		Real-time event 62
SIG63         Yes	Yes	Yes		Real-time event 63
SIGCANCEL     No	No	Yes		LWP internal signal
SIG32         Yes	Yes	Yes		Real-time event 32
SIG64         Yes	Yes	Yes		Real-time event 64
SIG65         Yes	Yes	Yes		Real-time event 65
SIG66         Yes	Yes	Yes		Real-time event 66
SIG67         Yes	Yes	Yes		Real-time event 67
SIG68         Yes	Yes	Yes		Real-time event 68
SIG69         Yes	Yes	Yes		Real-time event 69
SIG70         Yes	Yes	Yes		Real-time event 70
SIG71         Yes	Yes	Yes		Real-time event 71
SIG72         Yes	Yes	Yes		Real-time event 72
SIG73         Yes	Yes	Yes		Real-time event 73
SIG74         Yes	Yes	Yes		Real-time event 74
SIG75         Yes	Yes	Yes		Real-time event 75
SIG76         Yes	Yes	Yes		Real-time event 76
SIG77         Yes	Yes	Yes		Real-time event 77
SIG78         Yes	Yes	Yes		Real-time event 78
SIG79         Yes	Yes	Yes		Real-time event 79
SIG80         Yes	Yes	Yes		Real-time event 80
SIG81         Yes	Yes	Yes		Real-time event 81
SIG82         Yes	Yes	Yes		Real-time event 82
SIG83         Yes	Yes	Yes		Real-time event 83
SIG84         Yes	Yes	Yes		Real-time event 84
SIG85         Yes	Yes	Yes		Real-time event 85
SIG86         Yes	Yes	Yes		Real-time event 86
SIG87         Yes	Yes	Yes		Real-time event 87
SIG88         Yes	Yes	Yes		Real-time event 88
SIG89         Yes	Yes	Yes		Real-time event 89
SIG90         Yes	Yes	Yes		Real-time event 90
SIG91         Yes	Yes	Yes		Real-time event 91
SIG92         Yes	Yes	Yes		Real-time event 92
SIG93         Yes	Yes	Yes		Real-time event 93
SIG94         Yes	Yes	Yes		Real-time event 94
SIG95         Yes	Yes	Yes		Real-time event 95
SIG96         Yes	Yes	Yes		Real-time event 96
SIG97         Yes	Yes	Yes		Real-time event 97
SIG98         Yes	Yes	Yes		Real-time event 98
SIG99         Yes	Yes	Yes		Real-time event 99
SIG100        Yes	Yes	Yes		Real-time event 100
SIG101        Yes	Yes	Yes		Real-time event 101
SIG102        Yes	Yes	Yes		Real-time event 102
SIG103        Yes	Yes	Yes		Real-time event 103
SIG104        Yes	Yes	Yes		Real-time event 104
SIG105        Yes	Yes	Yes		Real-time event 105
SIG106        Yes	Yes	Yes		Real-time event 106
SIG107        Yes	Yes	Yes		Real-time event 107
SIG108        Yes	Yes	Yes		Real-time event 108
SIG109        Yes	Yes	Yes		Real-time event 109
SIG110        Yes	Yes	Yes		Real-time event 110
SIG111        Yes	Yes	Yes		Real-time event 111
SIG112        Yes	Yes	Yes		Real-time event 112
SIG113        Yes	Yes	Yes		Real-time event 113
SIG114        Yes	Yes	Yes		Real-time event 114
SIG115        Yes	Yes	Yes		Real-time event 115
SIG116        Yes	Yes	Yes		Real-time event 116
SIG117        Yes	Yes	Yes		Real-time event 117
SIG118        Yes	Yes	Yes		Real-time event 118
SIG119        Yes	Yes	Yes		Real-time event 119
SIG120        Yes	Yes	Yes		Real-time event 120
SIG121        Yes	Yes	Yes		Real-time event 121
SIG122        Yes	Yes	Yes		Real-time event 122
SIG123        Yes	Yes	Yes		Real-time event 123
SIG124        Yes	Yes	Yes		Real-time event 124
SIG125        Yes	Yes	Yes		Real-time event 125
SIG126        Yes	Yes	Yes		Real-time event 126
SIG127        Yes	Yes	Yes		Real-time event 127
SIGINFO       Yes	Yes	Yes		Information request
EXC_BAD_ACCESS Yes	Yes	Yes		Could not access memory
EXC_BAD_INSTRUCTION Yes	Yes	Yes		Illegal instruction/operand
EXC_ARITHMETIC Yes	Yes	Yes		Arithmetic exception
EXC_EMULATION Yes	Yes	Yes		Emulation instruction
EXC_SOFTWARE  Yes	Yes	Yes		Software generated exception
EXC_BREAKPOINT Yes	Yes	Yes		Breakpoint
SIGLIBRT      No	No	Yes		librt internal signal

sigset: 0x4



# fail statusctl

22.808000: ti->v:  - hexdump(len=20): 1e 00 00 00 1e 00 00 20 00 08 10 00 00 00 00 00 00 00 06 00

# OK status ctl

15.526000: ti->v:  - hexdump(len=20): 1e 00 00 00 1e 00 00 20 00 08 10 00 00 00 00 00 00 00 06 00

# disabling data cache while processing rx queues makes us assoc

```c
diff --git a/src/drivers/adapt-ip/rx.c b/src/drivers/adapt-ip/rx.c
index 9233ae4e19aa..932238251dd7 100644
--- a/src/drivers/adapt-ip/rx.c
+++ b/src/drivers/adapt-ip/rx.c
@@ -244,6 +244,7 @@ void aip_rx_int(long unsigned int data)

        spin_lock_bh(&q->lock);

+       Xil_DCacheDisable();
        ridx = aip_q_reg_read(aip, &q->regs->ridx);
        while (ridx != aip_q_reg_read(aip, &q->regs->widx)) {

@@ -386,6 +387,7 @@ next:
                 ridx = (ridx + 1) % q->len;
        };

+       Xil_DCacheEnable();
        aip_q_reg_write(aip, &q->regs->ridx, ridx);
        /* TODO: probably ack the ISR bits here */
        spin_unlock_bh(&q->lock);
```

# can't assoc wtf

revert:
- sw_reg: no assoc
- aip_zmalloc:  assoc
- sw_reg + aip_zmalloc: works
- `Revert "aip: remove sw_reg[]`": can't assoc
- + `Revert "aip: allocate from heap with aip_zmalloc()/aip_free()"`:  can assoc

# disable fcs writeback

https://chat.adapt-ip.com/direct/Sb9qjnsNAE2MxDLsCqLznoiMuArFGPYhyB?msg=dCjqaLGuqwwEci2ki

bit 8 of register 3

Wireless Interface:
        IPv4 Address:   10.11.11.11
        netmask:        255.255.255.0
        gateway:        10.11.11.1
        dns:            208.67.222.222


# 1294
## linux good

root@STA04:~# cat /sys/kernel/debug/ieee80211/phy0/aip/reg_dump
ID                   0x450E0200
Scratch              0x00000000
ConfigReg            0x770B000B
SettingsReg          0x120C0000
ScrambInitial        0x0000007F
AGC_settings0        0x8509080C
AGC_settings1        0x3B3B0A00
AGC_settings2        0x0295E84C
BB_MiscReg           0x00000000
StatsReg1            0x29CBD50F
Rx_Power             0x07846058
Intr_Status          0x00000080
Intr_Enable          0x0000A405
SPI1                 0x00FFFFFF
SPI2                 0x00000000
Beacon_base          0x00000000
BeaconReset          0x00000000
BeaconValue          0x00000000
TxQ0_base            0x38084000
TxQ0_mask            0x0000003F
TxQ0_ridx            0x00000006
TxQ0_widx            0x00000006
TxQ1_base            0x380C5000
TxQ1_mask            0x0000003F
TxQ1_ridx            0x00000000
TxQ1_widx            0x00000000
TxQ2_base            0x38106000
TxQ2_mask            0x0000003F
TxQ2_ridx            0x0000002E
TxQ2_widx            0x0000002E
TxQ3_base            0x38147000
TxQ3_mask            0x0000003F
TxQ3_ridx            0x00000000
TxQ3_widx            0x00000000
RxQ_base             0x38041000
RxQ_mask             0x0000003F
RxQ_ridx             0x0000002B
RxQ_widx             0x0000002B
Tx_reg_wrDone        0x00000000
TxVecWrDoneTS        0x0E9214A2
TxEndTS              0x0E921608
RxStartTS            0x0F4F5F68
TSF_Hi               0x00000000
TSF_Lo               0x0F5657FA
TxStartDelta         0x00000000
TxSigCRC             0x00000000
BB_RF_VGA            0x00000000
BB_RF_CTRL           0x00000006
TxDataCRC            0x89336514
RxDataCRC            0x433B56DC
SoftwareReset        0x00000000
Loci_base            0x00000000
Loci_length          0x00000000
WaitForAck           0x00000000
ConfigError          0x00000000
ConfigErrorHist      0x00000000
PowerUp_us           0x00000000
Scratch1             0x04001F20
TxStarvation         0x00000000
RxQ_overflow         0x38083000
CCA_Training_TO      0x00000000
CCA_Receiving_TO     0x00000000
GPIO                 0x00000002
Scratch3             0x00000000
TxReg0               0x00000000
TxReg1               0x00000000
TxReg2               0x00000000
TxReg3               0x00000000
TxReg4               0x00000000
RxReg0               0x00000065
RxReg1               0x00400065
RxReg2               0x00100000
RxReg3               0x00000000
RxReg4               0x00000000
M_MAC0               0x5EFFEE04
M_MAC1               0x000010C6
T_MAC0               0x00000000
T_MAC1               0x00000000
BSSID0               0x5EFFEE14
BSSID1               0x000010C6
aSIFSTime            0x0000008E
aSlotTime            0x00000034
aCCATime             0x00000000
aRxTxTurnaround      0x0000000C
aAirPropogation      0x00000001
ACKTxTime            0x000003E8
aMaxRetries          0x00000000
TxQ0_delay           0x002F3202
TxQ1_delay           0x005E4302
TxQ2_delay           0x0000A403
TxQ3_delay           0x0000A407
aMaxPacketTime       0x00326978
nTxPackets           0x000004DE
nTxAcks              0x00000210
nRxPackets           0x00000281
nRxAcks              0x000001FF
nTxNDPs              0x00000000
nRetries             0x0000026A
nTxBeacons           0x00000000
spare_a              0x00000074
spare_b              0x38106168
spare_c              0x38041158
spare_d              0x10000077
spare_e              0x00000000
TxRaw_base           0x3818A000
TxRaw_mask           0x00000001
TxRaw_ridx           0x00000000
TxRaw_widx           0x00000000
RxRaw_base           0x38189000
RxRaw_mask           0x00000001
RxRaw_ridx           0x00000000
RxRaw_widx           0x00000000
BeaconAlert          0x00000000
Random_val           0x02798967
aTSFPropogation      0x000003A2
AC_BK_val            0xA0000003
AC_BE_val            0xA0000003
AC_VI_val            0xA000000F
AC_VO_val            0xA0000007
BSS_Settings         0x00000040
LTFS_Settings1       0x14514514
nRxBeacons           0x0000001E
aRTSThreshold        0x00000000
RxLoci_base          0x3818B000
RxLoci_mask          0x00000003
RxLoci_ridx          0x00000000
RxLoci_widx          0x00000000
NAV_Timer            0x00000000
nRTSNacks            0x00000000
PAID                 0x00A1002A
TxScale              0x00008000
TSF_Diff_Lo          0xFFFFFFF5
TSF_Diff_Hi          0xFFFFFFFF
RxNDPpayloadH        0x00000000
RxNDPpayloadL        0x0024DA04
StartingSequenceNumber 0x000005F7
RxExpected           0x01008900
MoreData             0x00000000
RadioRaw_base        0x38200000
RadioRaw_post        0x00000000
RadioRaw_trigger     0x00000000
RadioRaw_TrigAddr    0x00000000
RadioRaw_LastAddr    0x00000000

# boot freertos

sete freertos_bootargs '-a -d0'

sete freertos_bootargs '-t -d1'
sete bootos freertos
sete bootfrom net
sete tftpdir phobos
boot

tftpboot 0x9000000 bot/freertos; bootelf 0x9000000 -a -a -x

# counter
```
	ntx = aip_reg_read(&aip->hal, AIP_REG_NUM_TX);
	nretries = aip_reg_read(&aip->hal, AIP_REG_NUM_RETRIES);
	per = (nretries * 100) / (ntx + nretries);
```

# aip qos
16.682000: wpa_tx on tid 0
aip sending on tid 0 ac 0, hwq 2
sending dscp 32
20.603000: wpa_tx on tid 1
aip sending on tid 1 ac 1, hwq 3
sending dscp 64
20.609000: wpa_tx on tid 2
aip sending on tid 2 ac 1, hwq 3
sending dscp 96
20.616000: wpa_tx on tid 3
aip sending on tid 3 ac 0, hwq 2
sending dscp 128
20.623000: wpa_tx on tid 4
aip sending on tid 4 ac 2, hwq 1
sending dscp 160
20.630000: wpa_tx on tid 5
aip sending on tid 5 ac 2, hwq 1
sending dscp 192
20.637000: wpa_tx on tid 6
aip sending on tid 6 ac 3, hwq 0
sending dscp 224
20.644000: wpa_tx on tid 7
aip sending on tid 7 ac 3, hwq 0

## registers

Reg #87 (offset 0x95C) = 0x00002172 # VO    
Reg #88 (offset 0x960) = 0x00003252 # VI
Reg #89 (offset 0x964) = 0x00008313 # BE
Reg #90 (offset 0x968) = 0x00008337 # BK


# mustafa

ad9361_trx_load_enable_fir() disable RX and TX filters



# segfaul


#0  0x0000556b954464a5 in uxListRemove (pxItemToRemove=0x7f44a0018c68) at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/list.c:196
#1  0x0000556b9544d407 in xTaskGenericNotifyFromISR (xTaskToNotify=0x7f44a0018c60, uxIndexToNotify=0x0, ulValue=0x0, eAction=eNoAction, pulPreviousNotificationValue=0x0, pxHigherPriorityTaskWoken=0x0) at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/tasks.c:5064
#2  0x0000556b95447350 in xStreamBufferSendFromISR (xStreamBuffer=0x7f44a000a600, pvTxData=0x7f44ae176308, xDataLengthBytes=0x8, pxHigherPriorityTaskWoken=0x0) at /home/thomas/dev/adapt-ip/phobos-hdp/FreeRTOS/FreeRTOS/Source/stream_buffer.c:682
#3  0x0000556b954d12f5 in frl_rx_thread (pvParam=0x7f44a0002520) at ../src/drivers/driver_fr_linux.c:126
#4  0x00007f44b0b48fa3 in start_thread (arg=<optimized out>) at pthread_create.c:486
#5  0x00007f44b0a794cf in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:95

# TX status

42 00 00 00 42 00 00 20 00 08 10 00 00 00 00 00 00 00 06 00

# make ubi vol

ubiformat /dev/mtd5 -y
ubiattach -m 5 -d 0 /dev/ubi_ctrl
ubimkvol /dev/ubi0 -N boardconf -m
ubiupdatevol /dev/ubiY_0 /path/to/Image.ubifs

# create jffs2

 flash_erase  -j /dev/mtd5 0 0
 mount -t jffs2 mtd5 foo

# HDP flash

n25q256a

# sta04 config

```
#!/bin/bash
#aip_set tx_att 36
aip_set tx_att 10

# turn off PA
reg=`aip_reg 2 | awk '{print $6}'`
aip_reg 2 $((reg & ~0x8))

#aip_set agc_mode m
#aip_set rx_gain 40

# raise idle level because channel rssi is high in cabled setup for some reason
#aip_power idle 50

#aip_power idle 62
#aip_power busy 32

aip_reg 119 0x40040

aip_set enable_fir 0
```


# wpa_cli

- wpa_supplicant/wpa_cli.c is the client
- src/common/wpa_ctrl.c is the transport
- wpa_supplicant/ctrl_iface.c is the control interface implementation
- wpa_supplicant/ctrl_iface_named_pipe.c is windows transport

# convert wpa_hexdump to wireshark parsable format

you have:
```
618.939000: wpa_tx WiFi-AH: - hexdump(len=62): 88 02 90 01 ff ff ff ff ff ff 10 c6 5e ff ee 03 00 00 00 00 00 00 50 01 00 00 aa aa 03 00 00 00 08 06 00 01 08 00 06 04 00 01 10 c6 5e ff ee 03 0a 0b 0b 0b 00 00 00 00 00 00 0a 0b 0b 0b
```

To convert to a format wireshark can understand:
```
echo <hexdump> | xxd -r -p | od -Ax -tx1 -v
```

Dump this into a file and import as (802.11) hexdump in wireshark.

# sta-config

/usr/local/wifi/sta_reset_default:/interface=wlan0
/usr/local/wifi/sta_reset_default:/prog=HaL
/usr/local/wifi/sta_preset_testparameters:/interface=wlan0
/usr/local/wifi/sta_preset_testparameters:/supplicant=0
/usr/local/wifi/sta_preset_testparameters:/mode=11ah
/usr/local/wifi/sta_set_wireless:/interface=wlan0
/usr/local/wifi/sta_set_wireless:/program=HaL
/usr/local/wifi/sta_set_wireless:/width=2
/usr/local/wifi/sta_set_wireless:/mcs_fixedrate=2
/usr/local/wifi/sta_set_wireless:/interface=wlan0
/usr/local/wifi/sta_set_wireless:/program=HaL
/usr/local/wifi/sta_set_wireless:/twt_reqsupport=enable
/usr/local/wifi/sta_set_wireless:/interface=wlan0
/usr/local/wifi/sta_set_wireless:/prog=HaL
/usr/local/wifi/sta_set_wireless:/ndppagingind=0
/usr/local/wifi/sta_set_wireless:/resppmmode=0
/usr/local/wifi/sta_set_wireless:/twt_setup=request
/usr/local/wifi/sta_set_wireless:/setupcommand=1
/usr/local/wifi/sta_set_wireless:/implicit=1
/usr/local/wifi/sta_set_wireless:/flowtype=0
/usr/local/wifi/sta_set_wireless:/wakeintervalexp=10
/usr/local/wifi/sta_set_wireless:/protection=0
/usr/local/wifi/sta_set_wireless:/nominalminwakedur=255
/usr/local/wifi/sta_set_wireless:/wakeintervalmantissa=512
/usr/local/wifi/sta_set_wireless:/twt_channel=0
/usr/local/wifi/sta_set_security:/interface=wlan0
/usr/local/wifi/sta_set_security:/ssid=HaL-5.2.28_380
/usr/local/wifi/sta_set_security:/type=OPEN
/usr/local/wifi/sta_associate:/ssid=HaL-5.2.28_380


# ap-config

/usr/local/wifi/ap_reset_default:/name=ap1hal
/usr/local/wifi/ap_reset_default:/program=HaL
/usr/local/wifi/ap_reset_default:/type=Testbed
/usr/local/wifi/ap_set_wireless:/name=ap1hal
/usr/local/wifi/ap_set_wireless:/program=HaL
/usr/local/wifi/ap_set_wireless:/channel=38
/usr/local/wifi/ap_set_wireless:/ssid=HaL-5.2.28_380
/usr/local/wifi/ap_set_wireless:/mode=11ah
/usr/local/wifi/ap_set_wireless:/bcnint=100
/usr/local/wifi/ap_set_security:/name=ap1hal
/usr/local/wifi/ap_set_security:/keymgnt=NONE
/usr/local/wifi/ap_set_wireless:/name=ap1hal
/usr/local/wifi/ap_set_wireless:/program=HaL
/usr/local/wifi/ap_set_wireless:/width=2
/usr/local/wifi/ap_set_wireless:/name=ap1hal
/usr/local/wifi/ap_set_wireless:/program=HaL
/usr/local/wifi/ap_set_wireless:/spatial_tx_stream=1SS
/usr/local/wifi/ap_set_wireless:/spatial_rx_stream=1SS
/usr/local/wifi/ap_set_wireless:/name=ap1hal
/usr/local/wifi/ap_set_wireless:/program=HaL
/usr/local/wifi/ap_set_wireless:/twt_respsupport=enable
/usr/local/wifi/ap_set_wireless:/name=ap1hal
/usr/local/wifi/ap_set_wireless:/program=HaL
/usr/local/wifi/ap_set_wireless:/mcs_fixedrate=2
/usr/local/wifi/ap_config_commit:/name=ap1hal

# WFA call


- currently at 86%, staff comfortable taking to NPI
- test week: 4/12-4/19
    - ACK bit ordering
    - additional work pending staff regression testing
- PF9: 5/3-5/14
