# connect to remote gdbserver from buildroot

launch.json:
```
{
	// Use IntelliSense to learn about possible attributes.
	// Hover to view descriptions of existing attributes.
	// For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
	"version": "0.2.0",
	"configurations": [
		{
			"name": "(gdb) connect",
			"type": "cppdbg",
			"request": "launch",
			"program": "/home/thomas/dev/adapt-ip/ahvt/output/build/sigma_dut-2021-09-15/sigma_dut",
			"args": [],
			"stopAtEntry": false,
			"cwd": "${fileDirname}",
			"miDebuggerPath": "/home/thomas/dev/adapt-ip/ahvt/output/host/bin/arm-linux-gdb",
			"miDebuggerServerAddress": "10.1.10.101:1234",
			"miDebuggerArgs": "-ix /home/thomas/dev/adapt-ip/ahvt/output/host/arm-buildroot-linux-gnueabihf/sysroot/usr/share/buildroot/gdbinit",
			"TargetArchitecture": "arm",
			"environment": [],
			"externalConsole": false,
			"MIMode": "gdb",
			"setupCommands": [
				{
					"description": "Enable pretty-printing for gdb",
					"text": "-enable-pretty-printing",
					"ignoreFailures": true
				}
			],
			 //"logging": { "engineLogging": true" }"
		}
	]
}
```
