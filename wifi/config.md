---
title: config
description: 
published: true
date: 2020-12-09T17:55:33.591Z
tags: 
editor: markdown
dateCreated: 2020-12-09T17:52:23.183Z
---

# WIFI configuration

## hostapd

**note:** this is not an exhaustive list, I've left off test only options and SAE EC groups selection.

### [hostapd.conf](http://git.campbell.adapt-ip.com/AIP/hostap/src/branch/master/hostapd/hostapd.conf)

| name                   | description                                                 |
|------------------------|-------------------------------------------------------------|
| `ctrl_interface`       | unix domain socket                                          |
| `interface`            | wifi interface                                              |
| `hw_mode`              | (ah)                                                        |
| `channel`              | primary channel                                             |
| `s1g_oper_channel`     | operating channel                                           |
| `multicast_to_unicast` | do multicast to unicast conversion (in linux)               |
| `ssid`                 | BSS SSID                                                    |
| `beacon_int`           | beacon interval, units of TU                                |
| `tsbtt_count`          | tsbtts in tbtt                                              |
| `s1g_capab`            | list of S1G capabiltiies to enable                          |
| `wmm_enabled`          | enable wmm (should be implied by hw_mode ah ?)              |
| `s1g_bss_color`        | BSS COLOR setting                                           |
| `s1g_tim_codec`        | TIM codec, block bitmap is default                          |
| `wpa`                  | WPA mode (1 is WPA2, 2 is WPA3)                             |
| `wpa_key_mgmt`         | `NONE`, `SAE`, `OWE`, etc.                                  |
| `ieee80211w`           | management frame protection disabled, optional, or required |
| `wpa_passphrase`       | passphrase                                                  |
| `wpa_pairwise`         | (CCMP) encryption cipher                                    |
| `sae_pwe`              | SAE PWE                                                     |
| `bridge`               | bridge interface which to add AP interface                  |
| `wds_sta`              | enable 4 address mode                                       |
| `ap_max_inactivity`    | BSS Max idle (?)                                            |


### hostapd_cli

| name                   | description              |
|------------------------|--------------------------|
| `deauthenticate <sta>` | deauthenticate given STA |


## wpa_supplicant

### [wpa_supplicant.conf](http://git.campbell.adapt-ip.com/AIP/hostap/src/branch/master/wpa_supplicant/wpa_supplicant.conf)

**global settings**:

| name             | description                    |
|------------------|--------------------------------|
| `ctrl_interface` | UNIX Domain Socket             |
| `ndp_probe`      | NDP Probe Request              |
| `passive_scan`   | do wildcard active scan or not |
| `sae_groups`     | SAE groups to use              |
| `sae_pwe`        | SAE PWE                        |

**network block settings**:

| name                | description                                 |
|---------------------|---------------------------------------------|
| `disabled`          | this network should be disabled by default  |
| `scan_ssid`         | send probe request for network SSID         |
| `scan_freq`         | list of frequencies to scan                 |
| `ssid`              | network SSID                                |
| `key_mgmt`          | `SAE` or `OPEN`, WPA2 has a different value |
| `s1g_sta_type`      | S1G STA type                                |
| `listen_int`        | STA Listen Interval to advertise            |
| `ctl_resp_1mhz`     | advertise support for 1MHz control response |
| `disable_s1g_ampdu` | override (disable) hardware A-MPDU support  |
| `ieee80211w`        | disable, support, or require 11w (MFP)      |
| `pairwise`          | pairwise cipher (`CCMP`)                    |
| `group`             | groupwise cipher (`CCMP`)                   |
| `psk`               | passphrase                                  |
| `twt_setup`         | set up TWT during assoc                     |
| `twt_setup_cmd`     | ?                                           |
| `twt_wake_int`      | TWT wake interval                           |
| `twt_min_wake_dur`  | TWT min. wake duration                      |

### wpa_cli

| name              | description                                                        |
|-------------------|--------------------------------------------------------------------|
| `scan_interval`   | scan interval in seconds                                           |
| `sta_autoconnect` | can disable reassoc                                                |
| `enable_network`  | enable network #, useful for setting options above before starting |
| `disconnect`      | disassociate from current network                                  |
| `reassociate`     | reassociate                                                        |

## iw

| name                     | description                      |
|--------------------------|----------------------------------|
| `phy0 set rts_threshold` | RTS threshold in bytes           |
| `phy0 set frag`          | fragmentation threshold in bytes |
| `wlan0 set power_save`   | enable power save (STA)          |

## aip

**aip_set**:

| name         | description                        |
|--------------|------------------------------------|
| `msg_gi`     | Guard Interval                     |
| `ack_1mhz`   | send 1Mhz ACK even on 2Mhz channel |
| `msg_format` | DUP_1MHZ or not                    |
| `mcs`        | fix MCS                            |
| `msg_agg`    | enable APPDU                       |
| `msg_cbw`    | fix message bandwidth              |
| `tx_att`     | TX attenuation in half dB          |
