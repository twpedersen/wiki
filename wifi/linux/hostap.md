# AP mode in wpa_supplicant

Need a network block for AP.

```c
enum wpas_mode {
	WPAS_MODE_INFRA = 0,
	WPAS_MODE_IBSS = 1,
	WPAS_MODE_AP = 2,
	WPAS_MODE_P2P_GO = 3,
	WPAS_MODE_P2P_GROUP_FORMATION = 4,
	WPAS_MODE_MESH = 5,
};
```
