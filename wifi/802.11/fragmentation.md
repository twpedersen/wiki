# 802.11 fragmentation

See 802.11-2016 10.2.7 and 10.5

- MSDU may span several fragments, each an MPDU
- each fragment is equal length, except the last
- More Data field =0 indicates the last fragment
- fragmented according to dot11FragmentationThreshold
- Sequence Control is the same for all fragments, except incrementing fragment number

- MPDUs are ACKed individually (10.2.7):
    > The MPDUs resulting from the fragmentation of an MSDU or MMPDU are
    > sent as independent transmissions, each of which is separately
    > acknowledged. This permits transmission retries to occur per
    > fragment, rather than
- can do fragment burst in a single TXOP
    - ACK overhead is SIFS + ACK, ~400us
    - average backoff is about 500us
    - save about 500us per fragment

## rts/cts

10.3.2.5 RTS/CTS with fragmentation

RTS only protects fragment 0 + ACK.
fragment 0 + ACK contain duration of fragment 1 + ACK.
fragment 1 + ACK contain duration of fragment 2 + ACK.
fragment 2 contains duration of ACK

## crypto

encryption happens after fragmentation (12.9.2.2 and Figure 5-1), so lower
mac will need key material.
