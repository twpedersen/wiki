# 11ah security requirements

## 11i

CCMP:

    counter mode with CBC-MAC (CCM) Protocol. The counter mode delivers data
    privacy while the CBC-MAC delivers data integrity and authentication.

    The AES standard uses 128-bit blocks for encryption, and for 802.11 the
    encryption key length is also fixed at 128 bits. Unlike TKIP, CCMP is
    mandatory for anyone implementing 802.11i.

## wpa3

see WPA3_Specification_v1.0.pdf
Also [this blog post](https://mrncciew.com/2019/11/29/wpa3-sae-mode/)

- SAE (cipher: CCM, akm: SHA256)
- PMF

### WPA3-Enterprise 192-bit

- PMF
- cipher: AES-256-GCM-SHA384
- akm:
    - TLS-ECDHE-ECDSA
    - TLS-ECDHE-RSA
    - TLS-DHE-RSA

ECDHE and ECDSA using the 384-bit prime modulus curve P-384
RSA >= 3072-bit modulus
DHE >= 3072-bit modulus

## 11ac

GCMP:

-   specified in 802.11ac

https://framebyframewifi.net/2016/08/02/802-11ac-encryption-upgrade

## 11w

management frame integrity (MIC only) AES-CMAC- BIP-CMAC-256
BIP-GMAC-128 BIP-GMAC-256

## 11ah

11ah: The CCMP header is not included in secure PV1 MPDUs, but
constructed locally at the STA as defined in 12.5.3.2a. For secure PV1
MPDUs, CCMP-128 processing expands the original MPDU size by 8 octets
for the MIC field. CCMP-256 processing expands the original MPDU size by
16 octets for the MIC field. Figure 12-16a depicts the PV1 MPDU when
using CCMP.

\>\> this means hardware needs to reconstruct the CCMP header, which
\>\> means hardware needs to store BPN (higher 4 bytes of PN) for each
\>\> STA

\>\> maybe on AP side we want to avoid any per-STA state? do crypto and
\>\> BA reorder in software to save HW (use STA chip for AP). \>\> we
could keep a limited number of STAs in hardware and page them in \>\> as
needed. This would limit us to do RAW only.

## needed AES ciphers

128 bit blocksize 128 or 256 bit keys

CCMP: CBC-MAC GCMP: GCM CMAC: CMAC GMAC: GMAC

## misc

CCMP MPDU: 0 26 27 28 29 30 31 32 33 34

  ------------- ----- ----- ------ -------------- ----- ----- ----- ----- --------- ----- -----
  MPDU header   PN0   PN1   rsvd   ext IV keyID   PN2   PN3   PN4   PN5   payload   MIC   FCS
  ------------- ----- ----- ------ -------------- ----- ----- ----- ----- --------- ----- -----

## mac80211

mac80211 cipher suites:

```
#define WLAN_CIPHER_SUITE_CCMP 0x000FAC04
#define WLAN_CIPHER_SUITE_WEP_104 0x000FAC05 
#define WLAN_CIPHER_SUITE_AES_CMAC 0x000FAC06
#define WLAN_CIPHER_SUITE_GCMP 0x000FAC08
#define WLAN_CIPHER_SUITE_GCMP_256 0x000FAC09
#define WLAN_CIPHER_SUITE_CCMP_256 0x000FAC0A
#define WLAN_CIPHER_SUITE_BIPGMAC_128 0x000FAC0B
#define WLAN_CIPHER_SUITE_BIPGMAC_256 0x000FAC0C
#define WLAN_CIPHER_SUITE_BIPCMAC_256 0x000FAC0D
#define WLAN_CIPHER_SUITE_SMS4 0x00147201
```

mac80211 AKM:
```
#define WLAN_AKM_SUITE_8021X			SUITE(0x000FAC, 1)
#define WLAN_AKM_SUITE_PSK			SUITE(0x000FAC, 2)
#define WLAN_AKM_SUITE_FT_8021X			SUITE(0x000FAC, 3)
#define WLAN_AKM_SUITE_FT_PSK			SUITE(0x000FAC, 4)
#define WLAN_AKM_SUITE_8021X_SHA256		SUITE(0x000FAC, 5)
#define WLAN_AKM_SUITE_PSK_SHA256		SUITE(0x000FAC, 6)
#define WLAN_AKM_SUITE_TDLS			SUITE(0x000FAC, 7)
#define WLAN_AKM_SUITE_SAE			SUITE(0x000FAC, 8)
#define WLAN_AKM_SUITE_FT_OVER_SAE		SUITE(0x000FAC, 9)
#define WLAN_AKM_SUITE_8021X_SUITE_B		SUITE(0x000FAC, 11)
#define WLAN_AKM_SUITE_8021X_SUITE_B_192	SUITE(0x000FAC, 12)
#define WLAN_AKM_SUITE_FILS_SHA256		SUITE(0x000FAC, 14)
#define WLAN_AKM_SUITE_FILS_SHA384		SUITE(0x000FAC, 15)
#define WLAN_AKM_SUITE_FT_FILS_SHA256		SUITE(0x000FAC, 16)
#define WLAN_AKM_SUITE_FT_FILS_SHA384		SUITE(0x000FAC, 17)
```
