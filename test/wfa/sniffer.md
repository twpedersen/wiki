# WFA sniffer and injector

[WFA Halow sniffer and injector presentation](https://cloud.adapt-ip.com/owncloud/remote.php/webdav/WFA/WFA_Test_Info/Wi-Fi_HaLow_Frame_Injector_and_Vendor_Sniffer_Agent_20200325.pptx)

![](2021-09-03-10-47-55.png)

## injector

Implemented by [wlantest from hostap](http://git.campbell.adapt-ip.com/AIP/hostap/src/branch/master/wlantest)

### packet injection on NL80211_IFTYPE_MONITOR

- works? test with ie. scapy

## sniffer

[sigma_dut](http://git.campbell.adapt-ip.com/AIP/sigma_dut) is both agent and sniffer.

> **TODO**: verify setup flow with wfa_sniffer, is upstream sigma_dut sufficient for wfa-halow?

### S1G radiotap header

The aip driver needs to prepend an S1G radiotap header at least on RX.

- [HaLow S1G radiotap spec](https://cloud.adapt-ip.com/owncloud/remote.php/webdav/WFA/WFA_Test_Info/Radiotap_Proposal_for_HaLow_v0.7.docx)
- [wireshark S1G radiotap parser](http://git.campbell.adapt-ip.com/AIP/wireshark/src/branch/master/epan/dissectors/packet-ieee80211-radiotap.c#L2622)

## wfa_sniffer

### requirements

```
# pp
sudo apt-get install libpar-packer-perl libnet-telnet-perl libshell-perl liblist-moreutils-perl libnet-openssh-perl
```

### build

```
pp -o wfa_sniffer ./script/wfa_sniffer.pl  pp -o wfa_sniffer ./script/wfa_sniffer.pl
```
