# open items from PF7 we need to care about

    Channel bandwidth for probe responses. The 802.11ah specification requires
    probe responses to be sent in the same channel bandwidth as that of the
    eliciting probe request but that is not always happening.
    [AI]: David to provide details and proposed changes for discussion in the TT/TG

We also send probe responses received @ 1Mhz out the 2Mhz channel if that is the operating.
Unclear how much work it is to support the correct implementattion.
**TOOD**: review hostap

    Staff provided the list of devices/equipment needed to bring up staff
    regression setup for Wi-Fi HaLow
    [AI]: Participants to reach out to staff/Giao and work out the details to
    make additional devices available to Staff

**Straff** is handling this.


    HaLow MAC/PHY testing - HaLow test plan security configuration, identify at
    least two test cases to be executed with OWE or SAE security configuration
    (fragmentation test, throughput test)
    [AI]: - TTG to discuss and update test plan and Staff to update scripts to
    remove open authentication configuration and replace with OWE or SAE
    security configuration as per the MRD requirement for HaLow (Section 6.3.1)
    [AI]: Staff to provide updated scripts for try outs; 4.2.16, 4.2.26,
    5.2.14, 5.2.22 with OWE and SAE security configurations

Yes at some point during the next PF we need to test these with OWE or SAE enabled.
