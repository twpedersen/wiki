# WFA testing

## components

- **UCC**: test executor running **wfa-halow**
- **wfa-halow**: the test scripts, includes **wts** and **wfa_sniffer**
- **wts**: the test framework executable
- **wfa_sniffer**: proxy between **UCC** and **sniffer**
- **sniffer agent**: runs **wfa_sniffer**, executes **tshark**, and hosts `/WTSSniffer` NFS share
- **sniffer**: actual wifi sniffer, we use a newracom provided board
- **TMS**: this is where test results are posted during events, see https://tms.wi-fi.org

### UCC

The UCC is responsible for executing the test scripts and should run Windows 10.

For the Windows 10 setup we install ssh and clone the **wfa-halow** repo.

See the [WFA test setup wiki page](http://docs.campbell.adapt-ip.com/books/wfa-a5b/page/wfa-test-setup#bkmrk-provisioning-a-new-u) for more details.

In the Adapt-IP campbell lab, our UCC login is `test@scout-win` password `test`.
Get a remote desktop by logging in to scout and running `virt-manager`.

### wfa-halow

upstream: `git@github.com:Wi-FiTestSuite/HaLow.git`
adapt-ip: `ssh://git@git.campbell.adapt-ip.com:2222/AIP/wfa-halow.git`

#### pulling in upstream changes

The branching strategy is periodically rebase on `upstream` and push-force to
master. The process is:

```bash
# update origin and upstream remotes
cd wfa-halow && git fetch --all

# new test branch
git checkout origin/master -B thomas/test

# rebase on upstream changes
git rebase upstream/master                  

# ... potentially resolve conflicts ...

# push your newly rebased adapt-ip master
git push origin thomas/test -f
```

Now on the UCC, switch to our test branch:

```bash
ssh test@scout-win
cd Desktop/wfa-halow && git fetch origin && git checkout origin/thomas/test

```

In another window, execute the WTS regression suites from scout:

``` bash
ssh scout
cd devops

# mounting /var/data/test/wts to /data inside the container will let us see the
# test results on http://test.campbell.adapt-ip.com
docker run -v /var/data/test/wts:/data -it  aip/testrunner "run_wts_tests -m wts_pass"
docker run -v /var/data/test/wts:/data -it  aip/testrunner "run_wts_tests -m wts_sec"
```

If you are happy with the results, push-force your test branch to master from
back on your workstation:

```bash
git push -f origin thomas/test:master
```



### WTS

`wts.exe` is provided as part of `wfa-halow` and lives in `Wi-FiTestSuite/UCC/bin`.

### sniffer agent / wfa_sniffer

`wfa_sniffer` proxies UCC with the actual halow sniffer. The `sniffer agent` host
provides an NFS share `/WTSSniffer` which the halow sniffer will mount and
write capture to.

`/etc/exports`:
```
/WTSSniffer 10.1.10.0/24(rw,sync,no_subtree_check)
```

`sniffer agent` also contains a wireshark install with the WFA changes to
support HaLow. Grab wireshark from the WFA FTP server, or [an adapt-ip
release](http://git.campbell.adapt-ip.com/attachments/a0b9c406-cbe2-4ad2-9e4b-b511a35df50f)

In our lab `sniffer agent` is `ub1604` as root. the `wfa_sniffer` and
`map.conf` both live in `wfa-halow/Wi-FiTestSuite/Sniffer/SAE`.  Copy these to
`ub1604:/WTSSniffer`. We use a small script to bring it up:

`/WTSSniffer/start_sniffer`:
```bash
#!/bin/bash -x
killall wfa_sniffer
touch /WTSSniffer/wfa_sniffer_logfile
/WTSSniffer/wfa_sniffer -host 10.1.10.72 -un pi -pw antjslan | tee > /WTSSniffer/wfa_sniffer_logfile &
```

Where `-host` takes the halow sniffer IP address.

### halow sniffer

We have a Newracom provided board in the lab. To get it ready for WTS testing:

```bash
ssh pi@10.1.10.72
sudo nohup /pf_sniffer_prep.sh
```

### TMS

[The TMS](https://tms.wi-fi.org) is where test results during engineering
events and plugfests are posted. 

```
username: halowuser
password: HaLowUser
```

We usually leave TMS disabled in `wfa-halow/Wi-FiTestSuite/UCC/config/TmsClient.conf`

**NOTE:** during event self-test we will enable TMS on `scout-win`, but don't
forget to disable or nightly tests will stomp over the event test results!

## how to run the tests

### manual runs

WTS tests are executed by `wts.exe` in **wfa-halow** on the **UCC**.
On the UCC: from within **wfa-halow**: 
    
    cd Wi-FiTestSuite/UCC/bin
    ./wts.exe HaL HaL-5.2.6

We've checked in a simple batch script `run_wts_batch <manifest>`, which just
loops over each line in the input file. The contents of `<manifest>` looks like:

```
HaL HaL-4.1.2
PMF PMF-4.3.3.3
WPA3-OWE OWE-4.2.2
WPA3-SAE SAE-4.2.6
```

### automating WTS

[devops.git](http://git.campbell.adapt-ip.com/AIP/devops) contains some helper
scripts for running the nightly WTS regression tests. It streamlines testbed
setup and [results
reporting](http://test.campbell.adapt-ip.com/wts/wts_pass/2021-01-25/). `scout`
in the campbell lab is already set up with the correct docker images and
network settings to run the tests. It also hosts `test.campbell.adapt-ip.com`,
so it makes sense to run the tests from there.

For more information, see `run_wts_tests -h`.

#### run a WTS suite

    ./scripts/run_wts_tests -m wts_pass

Or, easiest to run the suite from within docker so correct keys etc. have already been set up:

```bash
docker run -v /var/data/test/wts:/data -it  aip/testrunner "run_wts_tests -m wts_pass"
```

#### run a custom WTS suite

```bash
docker run -v /var/data/test/wts:/data -v $PWD/scripts/test_manifest:/test -it  aip/testrunner "run_wts_tests -m /test"
```

## test configuration

Testbed configuration is done per suite in an `AllInitConfig.txt`, ie:

```
./Wi-FiTestSuite/UCC/cmds/WTS-HaL/AllInitConfig_HaL.txt
./Wi-FiTestSuite/UCC/cmds/WTS-PMF/AllInitConfig_PMF.txt
./Wi-FiTestSuite/UCC/cmds/WTS-WPA3/OWE/AllInitConfig_OWE.txt
./Wi-FiTestSuite/UCC/cmds/WTS-WPA3/SAE/AllInitConfig_WPA3-SAE.txt
```

Each test has a `DUT` and one or more testbed devices (`STA1`, `AP`, etc.). We
also need to specify the correct sniffer agent IP address. For example the
important settings are:

```
# DUT Control Agent
wfa_control_agent_dut!ipaddr=10.10.5.1,port=9000!

# Wireless IP of DUT
dut_wireless_ip!192.165.100.50!
...

wfa_control_agent_ap1hal_ap!ipaddr=10.10.1.1,port=9000!
define!$ap1hal_ap_wireless_ip!192.165.100.1!
...
wfa_sniffer!ipaddr=10.10.10.75,port=9999!
```

This specifies `10.10.5.1` as the `DUT`(in this case `STAUT`) with a single
`AP1`. In other words this test is valid for a STA test (`5.x.x`). In case of a
security test you also need to specify `wfa_console_tg` as the AP Halow IP
address, though this should be phased out in future `wfa-halow`.

### HaLow IP configuration

STA device will self configure their HaLow IP (`dut_wireless_ip` or
`sta1hal_sta_wireless_ip`). APs will not. This means the STA Halow address in
`AllInitConfig.txt` can be whatever you want as long as it does not match the
AP HaLow address. The AP HaLow address must match the vendor configuration.

### TMS

TMS config is in `Wi-FiTestSuite/UCC/config/TmsClient.conf`, and should remain
valid for future event. Upload is disabled by default, but to toggle:

```diff
diff --git a/Wi-FiTestSuite/UCC/config/TmsClient.conf b/Wi-FiTestSuite/UCC/config/TmsClient.conf
index babc9aa15a39..0eaeb0b018bf 100644
--- a/Wi-FiTestSuite/UCC/config/TmsClient.conf
+++ b/Wi-FiTestSuite/UCC/config/TmsClient.conf
@@ -3,11 +3,11 @@
 #########################
 #To enable TMS update after UCC run
 #TMS : Enabled/Disabled
-TMS_feature=Enabled
+TMS_feature=Disabled

 #if only need to disable update result to FTP..
 #FTP : Enabled/Disabled
-FTP_feature=Enabled
+FTP_feature=Disabled

 #########################
 # TMS Server info
```

### WFA HaLow resources

- [documents](https://groups.wi-fi.org/apps/org/workgroup/wifihalow-tg/)
- [calendar](https://groups.wi-fi.org/apps/org/workgroup/wifihalow-tg/calendar.php)

### other resources

- [adapt-ip test status](https://docs.google.com/spreadsheets/d/1hPvTEf089GhbPFwYFs-0UELi-rNWCZx1L315kdSxWXA/edit?usp=sharing)
- [WFA testbed spreadsheet](https://docs.google.com/spreadsheets/d/1B8YrsGjucE0SFa8A3byvvaA76qHQA2JJVP_3zSgwNBc/edit#gid=0)
- [WFA testbed page](http://docs.campbell.adapt-ip.com/books/wfa-a5b/page/wfa-test-setup)

# misc
## wireshark over ssh

    # ssh <UCC laptop> ssh <sniffer> ...
    ssh test@10.1.15.3 ssh pi@10.10.10.70 sudo tshark -i wlan0 -w - | wireshark -i - -k

