# testbeds

## nightly

### sta14

```
sete ipaddr 10.1.10.42
sete serverip 10.1.10.20
sete ethaddr 00:0a:35:00:01:22
sete hostname STA14
sete tftpdir bot
sete bootfrom net_initramfs
```

### sta04

```
sete ipaddr 10.1.10.104
sete serverip 10.1.10.20
sete ethaddr 00:0a:35:00:01:23
sete hostname STA04
sete tftpdir bot
sete bootfrom net_initramfs
```
