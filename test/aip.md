# Adapt-IP regression testing

## testbeds
## testbed locking

Each testbed is defined by a testbed configuration file `scripts/tb/<testbed
name>.json` in [devops.git](http://git.campbell.adapt-ip.com/AIP/devops). It
looks something like:

```json
{
    "name": "nightly",
    "nodes": [
        {
            "hostname": "sta04",
            "power": "http://webpower2/outlet?6=CCL",
            "power_login": "bot:bot"
        },
        {
            "hostname": "sta14",
            "power": "http://webpower2/outlet?7=CCL",
            "power_login": "bot:bot"

        }
    ]
}
```

The semantics should be fairly obvious. For testbed exclusion, users of the
testbeds (`aip_wtf`, `wts`, etc.) are expected to take a lockfile on `data`
before accessing the testbed. The `testbed` script in `devops.git` can be used
for this:

    ./scripts/testbed lock nightly
